
package client.create;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the client.create package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: client.create
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link UpdateClient }
     * 
     */
    public UpdateClient createUpdateClient() {
        return new UpdateClient();
    }

    /**
     * Create an instance of {@link Client }
     * 
     */
    public Client createClient() {
        return new Client();
    }

    /**
     * Create an instance of {@link CreateClientResponse }
     * 
     */
    public CreateClientResponse createCreateClientResponse() {
        return new CreateClientResponse();
    }

    /**
     * Create an instance of {@link GetClientResponse }
     * 
     */
    public GetClientResponse createGetClientResponse() {
        return new GetClientResponse();
    }

    /**
     * Create an instance of {@link UpdateClientResponse }
     * 
     */
    public UpdateClientResponse createUpdateClientResponse() {
        return new UpdateClientResponse();
    }

    /**
     * Create an instance of {@link BPELCreateClientRequest }
     * 
     */
    public BPELCreateClientRequest createBPELCreateClientRequest() {
        return new BPELCreateClientRequest();
    }

    /**
     * Create an instance of {@link DeleteClientResponse }
     * 
     */
    public DeleteClientResponse createDeleteClientResponse() {
        return new DeleteClientResponse();
    }

    /**
     * Create an instance of {@link BPELCreateClientResponse }
     * 
     */
    public BPELCreateClientResponse createBPELCreateClientResponse() {
        return new BPELCreateClientResponse();
    }

    /**
     * Create an instance of {@link GetClient }
     * 
     */
    public GetClient createGetClient() {
        return new GetClient();
    }

    /**
     * Create an instance of {@link DeleteClient }
     * 
     */
    public DeleteClient createDeleteClient() {
        return new DeleteClient();
    }

    /**
     * Create an instance of {@link CreateClient }
     * 
     */
    public CreateClient createCreateClient() {
        return new CreateClient();
    }

    /**
     * Create an instance of {@link Trainer }
     * 
     */
    public Trainer createTrainer() {
        return new Trainer();
    }

    /**
     * Create an instance of {@link ValidateTrainerFormResponse }
     * 
     */
    public ValidateTrainerFormResponse createValidateTrainerFormResponse() {
        return new ValidateTrainerFormResponse();
    }

    /**
     * Create an instance of {@link ValidateClientForm }
     * 
     */
    public ValidateClientForm createValidateClientForm() {
        return new ValidateClientForm();
    }

    /**
     * Create an instance of {@link ValidateClientFormResponse }
     * 
     */
    public ValidateClientFormResponse createValidateClientFormResponse() {
        return new ValidateClientFormResponse();
    }

    /**
     * Create an instance of {@link ValidateTrainerForm }
     * 
     */
    public ValidateTrainerForm createValidateTrainerForm() {
        return new ValidateTrainerForm();
    }

}
