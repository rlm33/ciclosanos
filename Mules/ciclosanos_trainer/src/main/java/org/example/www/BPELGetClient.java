package org.example.www;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import javax.xml.ws.Service;

/**
 * This class was generated by Apache CXF 2.5.1
 * 2014-05-25T18:33:24.707+02:00
 * Generated source version: 2.5.1
 * 
 */
@WebServiceClient(name = "BPELGetClient", 
                  wsdlLocation = "http://localhost:8080/ode/processes/BPELGetClient?wsdl",
                  targetNamespace = "http://www.example.org") 
public class BPELGetClient extends Service {

    public final static URL WSDL_LOCATION;

    public final static QName SERVICE = new QName("http://www.example.org", "BPELGetClient");
    public final static QName BPELGetClientPort = new QName("http://www.example.org", "BPELGetClientPort");
    static {
        URL url = null;
        try {
            url = new URL("http://localhost:8080/ode/processes/BPELGetClient?wsdl");
        } catch (MalformedURLException e) {
            java.util.logging.Logger.getLogger(BPELGetClient.class.getName())
                .log(java.util.logging.Level.INFO, 
                     "Can not initialize the default wsdl from {0}", "http://localhost:8080/ode/processes/BPELGetClient?wsdl");
        }
        WSDL_LOCATION = url;
    }

    public BPELGetClient(URL wsdlLocation) {
        super(wsdlLocation, SERVICE);
    }

    public BPELGetClient(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public BPELGetClient() {
        super(WSDL_LOCATION, SERVICE);
    }
    
    //This constructor requires JAX-WS API 2.2. You will need to endorse the 2.2
    //API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS 2.1
    //compliant code instead.
    public BPELGetClient(WebServiceFeature ... features) {
        super(WSDL_LOCATION, SERVICE, features);
    }

    //This constructor requires JAX-WS API 2.2. You will need to endorse the 2.2
    //API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS 2.1
    //compliant code instead.
    public BPELGetClient(URL wsdlLocation, WebServiceFeature ... features) {
        super(wsdlLocation, SERVICE, features);
    }

    //This constructor requires JAX-WS API 2.2. You will need to endorse the 2.2
    //API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS 2.1
    //compliant code instead.
    public BPELGetClient(URL wsdlLocation, QName serviceName, WebServiceFeature ... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     *
     * @return
     *     returns BPELGetClientPortType
     */
    @WebEndpoint(name = "BPELGetClientPort")
    public BPELGetClientPortType getBPELGetClientPort() {
        return super.getPort(BPELGetClientPort, BPELGetClientPortType.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns BPELGetClientPortType
     */
    @WebEndpoint(name = "BPELGetClientPort")
    public BPELGetClientPortType getBPELGetClientPort(WebServiceFeature... features) {
        return super.getPort(BPELGetClientPort, BPELGetClientPortType.class, features);
    }

}
