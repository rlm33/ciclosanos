
package org.example.www;

/**
 * Please modify this class to meet your needs
 * This class is not complete
 */

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;

/**
 * This class was generated by Apache CXF 2.5.1
 * 2014-05-25T17:47:36.664+02:00
 * Generated source version: 2.5.1
 * 
 */
public final class BPELDeleteTrainerPortType_BPELDeleteTrainerPort_Client {

    private static final QName SERVICE_NAME = new QName("http://www.example.org", "BPELDeleteTrainer");

    private BPELDeleteTrainerPortType_BPELDeleteTrainerPort_Client() {
    }

    public static void main(String args[]) throws java.lang.Exception {
        URL wsdlURL = BPELDeleteTrainer.WSDL_LOCATION;
        if (args.length > 0 && args[0] != null && !"".equals(args[0])) { 
            File wsdlFile = new File(args[0]);
            try {
                if (wsdlFile.exists()) {
                    wsdlURL = wsdlFile.toURI().toURL();
                } else {
                    wsdlURL = new URL(args[0]);
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
      
        BPELDeleteTrainer ss = new BPELDeleteTrainer(wsdlURL, SERVICE_NAME);
        BPELDeleteTrainerPortType port = ss.getBPELDeleteTrainerPort();  
        
        {
        System.out.println("Invoking process...");
        org.example.www.BPELDeleteTrainerRequest _process_payload = null;
        org.example.www.BPELDeleteTrainerResponse _process__return = port.process(_process_payload);
        System.out.println("process.result=" + _process__return);


        }

        System.exit(0);
    }

}
