
package org.example.www;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="exists" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="trainer" type="{http://www.example.org}Trainer" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "exists",
    "trainer"
})
@XmlRootElement(name = "BPELGetTrainerResponse")
public class BPELGetTrainerResponse {

    @XmlElement(namespace = "http://www.example.org")
    protected boolean exists;
    @XmlElement(namespace = "http://www.example.org")
    protected Trainer trainer;

    /**
     * Gets the value of the exists property.
     * This getter has been renamed from isExists() to getExists() by cxf-xjc-boolean plugin.
     * 
     */
    public boolean getExists() {
        return exists;
    }

    /**
     * Sets the value of the exists property.
     * 
     */
    public void setExists(boolean value) {
        this.exists = value;
    }

    /**
     * Gets the value of the trainer property.
     * 
     * @return
     *     possible object is
     *     {@link Trainer }
     *     
     */
    public Trainer getTrainer() {
        return trainer;
    }

    /**
     * Sets the value of the trainer property.
     * 
     * @param value
     *     allowed object is
     *     {@link Trainer }
     *     
     */
    public void setTrainer(Trainer value) {
        this.trainer = value;
    }

}
