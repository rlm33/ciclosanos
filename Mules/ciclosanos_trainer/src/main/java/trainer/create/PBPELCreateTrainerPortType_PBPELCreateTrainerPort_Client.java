
package trainer.create;

/**
 * Please modify this class to meet your needs
 * This class is not complete
 */

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;

/**
 * This class was generated by Apache CXF 2.5.1
 * 2014-05-26T11:18:18.023+02:00
 * Generated source version: 2.5.1
 * 
 */
public final class PBPELCreateTrainerPortType_PBPELCreateTrainerPort_Client {

    private static final QName SERVICE_NAME = new QName("http://www.example.org", "PBPELCreateTrainer");

    private PBPELCreateTrainerPortType_PBPELCreateTrainerPort_Client() {
    }

    public static void main(String args[]) throws java.lang.Exception {
        URL wsdlURL = PBPELCreateTrainer.WSDL_LOCATION;
        if (args.length > 0 && args[0] != null && !"".equals(args[0])) { 
            File wsdlFile = new File(args[0]);
            try {
                if (wsdlFile.exists()) {
                    wsdlURL = wsdlFile.toURI().toURL();
                } else {
                    wsdlURL = new URL(args[0]);
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
      
        PBPELCreateTrainer ss = new PBPELCreateTrainer(wsdlURL, SERVICE_NAME);
        PBPELCreateTrainerPortType port = ss.getPBPELCreateTrainerPort();  
        
        {
        System.out.println("Invoking process...");
        trainer.create.PBPELCreateTrainerRequest _process_payload = null;
        trainer.create.PBPELCreateTrainerResponse _process__return = port.process(_process_payload);
        System.out.println("process.result=" + _process__return);


        }

        System.exit(0);
    }

}
