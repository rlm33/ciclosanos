
package trainer.create;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the trainer.create package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: trainer.create
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CreateTrainerResponse }
     * 
     */
    public CreateTrainerResponse createCreateTrainerResponse() {
        return new CreateTrainerResponse();
    }

    /**
     * Create an instance of {@link GetTrainerResponse }
     * 
     */
    public GetTrainerResponse createGetTrainerResponse() {
        return new GetTrainerResponse();
    }

    /**
     * Create an instance of {@link Trainer }
     * 
     */
    public Trainer createTrainer() {
        return new Trainer();
    }

    /**
     * Create an instance of {@link PBPELCreateTrainerRequest }
     * 
     */
    public PBPELCreateTrainerRequest createPBPELCreateTrainerRequest() {
        return new PBPELCreateTrainerRequest();
    }

    /**
     * Create an instance of {@link CreateTrainer }
     * 
     */
    public CreateTrainer createCreateTrainer() {
        return new CreateTrainer();
    }

    /**
     * Create an instance of {@link UpdateTrainer }
     * 
     */
    public UpdateTrainer createUpdateTrainer() {
        return new UpdateTrainer();
    }

    /**
     * Create an instance of {@link DeleteTrainer }
     * 
     */
    public DeleteTrainer createDeleteTrainer() {
        return new DeleteTrainer();
    }

    /**
     * Create an instance of {@link DeleteTrainerResponse }
     * 
     */
    public DeleteTrainerResponse createDeleteTrainerResponse() {
        return new DeleteTrainerResponse();
    }

    /**
     * Create an instance of {@link PBPELCreateTrainerResponse }
     * 
     */
    public PBPELCreateTrainerResponse createPBPELCreateTrainerResponse() {
        return new PBPELCreateTrainerResponse();
    }

    /**
     * Create an instance of {@link GetTrainer }
     * 
     */
    public GetTrainer createGetTrainer() {
        return new GetTrainer();
    }

    /**
     * Create an instance of {@link UpdateTrainerResponse }
     * 
     */
    public UpdateTrainerResponse createUpdateTrainerResponse() {
        return new UpdateTrainerResponse();
    }

    /**
     * Create an instance of {@link Client }
     * 
     */
    public Client createClient() {
        return new Client();
    }

    /**
     * Create an instance of {@link ValidateTrainerFormResponse }
     * 
     */
    public ValidateTrainerFormResponse createValidateTrainerFormResponse() {
        return new ValidateTrainerFormResponse();
    }

    /**
     * Create an instance of {@link ValidateClientForm }
     * 
     */
    public ValidateClientForm createValidateClientForm() {
        return new ValidateClientForm();
    }

    /**
     * Create an instance of {@link ValidateClientFormResponse }
     * 
     */
    public ValidateClientFormResponse createValidateClientFormResponse() {
        return new ValidateClientFormResponse();
    }

    /**
     * Create an instance of {@link ValidateTrainerForm }
     * 
     */
    public ValidateTrainerForm createValidateTrainerForm() {
        return new ValidateTrainerForm();
    }

}
