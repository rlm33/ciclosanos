package trainer.create;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import javax.xml.ws.Service;

/**
 * This class was generated by Apache CXF 2.5.1
 * 2014-05-26T11:18:18.112+02:00
 * Generated source version: 2.5.1
 * 
 */
@WebServiceClient(name = "PBPELCreateTrainer", 
                  wsdlLocation = "http://localhost:8080/ode/processes/PBPELCreateTrainer?wsdl",
                  targetNamespace = "http://www.example.org") 
public class PBPELCreateTrainer extends Service {

    public final static URL WSDL_LOCATION;

    public final static QName SERVICE = new QName("http://www.example.org", "PBPELCreateTrainer");
    public final static QName PBPELCreateTrainerPort = new QName("http://www.example.org", "PBPELCreateTrainerPort");
    static {
        URL url = null;
        try {
            url = new URL("http://localhost:8080/ode/processes/PBPELCreateTrainer?wsdl");
        } catch (MalformedURLException e) {
            java.util.logging.Logger.getLogger(PBPELCreateTrainer.class.getName())
                .log(java.util.logging.Level.INFO, 
                     "Can not initialize the default wsdl from {0}", "http://localhost:8080/ode/processes/PBPELCreateTrainer?wsdl");
        }
        WSDL_LOCATION = url;
    }

    public PBPELCreateTrainer(URL wsdlLocation) {
        super(wsdlLocation, SERVICE);
    }

    public PBPELCreateTrainer(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public PBPELCreateTrainer() {
        super(WSDL_LOCATION, SERVICE);
    }
    
    //This constructor requires JAX-WS API 2.2. You will need to endorse the 2.2
    //API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS 2.1
    //compliant code instead.
    public PBPELCreateTrainer(WebServiceFeature ... features) {
        super(WSDL_LOCATION, SERVICE, features);
    }

    //This constructor requires JAX-WS API 2.2. You will need to endorse the 2.2
    //API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS 2.1
    //compliant code instead.
    public PBPELCreateTrainer(URL wsdlLocation, WebServiceFeature ... features) {
        super(wsdlLocation, SERVICE, features);
    }

    //This constructor requires JAX-WS API 2.2. You will need to endorse the 2.2
    //API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS 2.1
    //compliant code instead.
    public PBPELCreateTrainer(URL wsdlLocation, QName serviceName, WebServiceFeature ... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     *
     * @return
     *     returns PBPELCreateTrainerPortType
     */
    @WebEndpoint(name = "PBPELCreateTrainerPort")
    public PBPELCreateTrainerPortType getPBPELCreateTrainerPort() {
        return super.getPort(PBPELCreateTrainerPort, PBPELCreateTrainerPortType.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns PBPELCreateTrainerPortType
     */
    @WebEndpoint(name = "PBPELCreateTrainerPort")
    public PBPELCreateTrainerPortType getPBPELCreateTrainerPort(WebServiceFeature... features) {
        return super.getPort(PBPELCreateTrainerPort, PBPELCreateTrainerPortType.class, features);
    }

}
