
package trainer.update;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="trainer" type="{http://www.example.org}Trainer"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "trainer"
})
@XmlRootElement(name = "BPELUpdateTrainerRequest")
public class BPELUpdateTrainerRequest {

    @XmlElement(namespace = "http://www.example.org", required = true)
    protected Trainer trainer;

    /**
     * Gets the value of the trainer property.
     * 
     * @return
     *     possible object is
     *     {@link Trainer }
     *     
     */
    public Trainer getTrainer() {
        return trainer;
    }

    /**
     * Sets the value of the trainer property.
     * 
     * @param value
     *     allowed object is
     *     {@link Trainer }
     *     
     */
    public void setTrainer(Trainer value) {
        this.trainer = value;
    }

}
