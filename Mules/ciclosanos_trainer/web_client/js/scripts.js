function getTrainer(email){
  var trainerAPI = "/trainer?email=" + email;

  $.get(trainerAPI)
  .done(function(data) {
	  console.log(data);
	  $('#show-trainer').show();
	  if (data.exists) {
		  $('#no-trainer-found').hide();
		  $('#trainer-email').text(data.trainer.email);
		  $('#trainer-name').text(data.trainer.name);
		  $('#trainer-data').show();
		  $('#editting-buttons').hide();
		  $('#normal-buttons').show();
		  $('#trainer-email-edit').val(data.trainer.email);
		  $('#trainer-name-edit').val(data.trainer.name);
		  $('#data-trainer').show();
		  $('#data-trainer-edit').hide();
		  return
	  }
	  $('#trainer-data').hide();
	  $('#no-trainer-found').show();
  });
}

function editTainerData(){
 	$('#editting-buttons').show();
 	$('#normal-buttons').hide();
 	$('#data-trainer').hide();
 	$('#data-trainer-edit').show();
}

function cancelEditTrainerData(){
	var trainerName = $('#trainer-name').text();
	var trainerEmail = $('#trainer-email').text();
	$('#trainer-email-edit').val(trainerEmail);
	$('#trainer-name-edit').val(trainerName);
	$('#editting-buttons').hide();
 	$('#normal-buttons').show();
 	$('#data-trainer').show();
 	$('#data-trainer-edit').hide();
}

function deleteTrainerData(){
	var trainerEmail = $('#gt-mail').val();
	var trainerAPI = "/trainer";
	$.post(trainerAPI,
		{
			email: trainerEmail
		}
	).done(function(d) {
		$('#trainer-data').hide();
	    $('#statusmessage').text('Trainer with email ' + trainerEmail + ' has been deleted...').animate({'margin-bottom':0},200);
	    setTimeout( function(){
	        $('#statusmessage').animate({'margin-bottom':-25},200);
	    }, 5*1000);
	});
	
}

function createTrainer(){
	var trainerName = $('#create-trainer-name').val();
	var trainerEmail = $('#create-trainer-email').val();
	
	$.post('/trainers', {
		name: trainerName,
		email: trainerEmail
	}).done(function(d) {
		console.log(d);
		$('#create-trainer-name').val("");
		$('#create-trainer-email').val("");
	    $('#statusmessage').text('Trainer with email ' + trainerEmail + ' has been created...').animate({'margin-bottom':0},200);
	    setTimeout( function(){
	        $('#statusmessage').animate({'margin-bottom':-25},200);
	    }, 5*1000);
	});
}

function getTrainerClick() {
  var email = $('#gt-mail').val();
  getTrainer(email);
}

function createTraining(){
		var trainingName = $('#create-training-name').val();
		
		$.post('/trainings', {
			name: trainingName
		}).done(function(d) {
			console.log(d);
			$('#create-training-name').val("");
		    $('#statusmessage').text('Training with name ' + trainingName + ' has been created...').animate({'margin-bottom':0},200);
		    setTimeout( function(){
		        $('#statusmessage').animate({'margin-bottom':-25},200);
		    }, 5*1000);
		});
	}

function updateTrainer() {
		var trainerAPI = '/trainer-update';
		var trainerName = $('#trainer-name-edit').val();
		var trainerEmail = $('#trainer-email-edit').val();
		
		$.post(trainerAPI, {
				name: trainerName,
				email: trainerEmail
			}
		).done(function(d) {
			console.log(d);
			$('#trainer-name').text(trainerName);
		    $('#statusmessage').text('Trainer with email ' + trainerEmail + ' has been updated...').animate({'margin-bottom':0},200);
		    setTimeout( function(){
		        $('#statusmessage').animate({'margin-bottom':-25},200);
		    }, 5*1000);
		    $('#editting-buttons').hide();
		 	$('#normal-buttons').show();
		 	$('#data-trainer').show();
		 	$('#data-trainer-edit').hide();
		});
	}
	
	function getAllTrainings() {
		var no_trainings = "<tr><td>There are no trainings</td></tr>";
		var $table_trainings = $('#table-trainings');
		$table_trainings.html("<tr><td>Loading...</td></tr>");
		$.get('/trainings').done(function(d) {
			data = $.parseJSON(d);
			if (data.length == 0) {
				$table_trainings.html(no_trainings);
				return;
			}
			var html = "<tr><th>Name</th><th>Popularity</th></tr>";
			$.each(data, function(i, training) {
				var action = "deleteTraining('"+training[0]+"')";
				html += "<tr><td>"+training[0]+"</td><td>"+training[1]+"</td><td><button class=\"btn\" onclick=\""+action+"; return false;\">Delete</button></td></tr>";
			});
			$table_trainings.html(html);
		});
	}
	
	function deleteTraining(training_name) {
		$.post('/training-delete', {
			name: training_name
		}).done(function(d){
			$('#statusmessage').text('Training with name ' + training_name+ ' has been delted...').animate({'margin-bottom':0},200);
		    setTimeout( function(){
		        $('#statusmessage').animate({'margin-bottom':-25},200);
		    }, 5*1000);
		    getAllTrainings();
		});
	}
	
	$(function(){
		getAllTrainings();
	});