var DisseasesTam = 0;
var InjuriesTam = 0;

$('#no-client-found').hide();
$('#client-data').hide();

$(document).ready(function () {
    
    $('#create-client-bday').datepicker({
        format: "yyyy-mm-dd"
    });  

});

function createClient() {
	var clientName = $('#create-client-name').val();
	var clientEmail = $('#create-client-email').val();
	var clientBday = $('#create-client-bday').val();
	console.log(clientBday);
	$.post('/client', {
		name: clientName,
		email: clientEmail,
		bday: clientBday
	}).done(function(d) {
		console.log(d);
		$('#create-client-name').val("");
		$('#create-client-email').val("");
		$('#create-client-bday').val("");
	    $('#statusmessage').text('Client with email ' + clientEmail + ' has been created...').animate({'margin-bottom':0},200);
	    setTimeout( function(){
	        $('#statusmessage').animate({'margin-bottom':-25},200);
	    }, 5*1000);
	});
}

function getClient() {
	var clientName = $('#gt-name').val();
	
	$.get('/client?name='+clientName).done(function(d){
		$('#show-client').show();
		if (d.exists) {
			  $('#no-client-found').hide();
			  $('#client-email').text(d.client.email);
    		  $('#client-name').text(d.client.name);
			  $('#client-data').show();
			  $('#editting-buttons').hide();
			  $('#normal-buttons').show();
			  $('#client-email-edit').val(d.client.email);
			  $('#client-name-edit').val(d.client.name);
			  $('#data-client').show();
			  $('#data-client-edit').hide();
			  return
		  }
		  $('#client-data').hide();
		  $('#no-client-found').show();
	});
}

function getAllTrainings() {
	var no_trainings = "<tr><td>There are no trainings</td></tr>";
	var $table_trainings = $('#table-trainings');
	$table_trainings.html("<tr><td>Loading...</td></tr>");
	$.get('/trainings').done(function(d) {
		data = $.parseJSON(d);
		if (data.length == 0) {
			$table_trainings.html(no_trainings);
			return;
		}
		var html = "<tr><th>Name</th><th>Popularity</th></tr>";
		$.each(data, function(i, training) {
			var pick = "pickTraining('"+training[0]+"')";
			var stop = "stopTraining('"+training[0]+"')";
			html += "<tr><td>"+training[0]+"</td><td>"+training[1]+"</td><td><button class=\"btn\" onclick=\""+pick+"; return false;\">Pick</button></td><td><button class=\"btn\" onclick=\""+stop+"; return false;\">Stop</button></td></tr>";
		});
		$table_trainings.html(html);
	});
}

function pickTraining(training_name) {
	if (!$('#client-name').text()) {
		alert("You must Get a Client first!");
	}
	$.post('/training-pick', {
		name: training_name,
		client: $('#client-name').text()
	}).done(function(d){
		getAllTrainings();
	});
}

function stopTraining(training_name) {
	if (!$('#client-name').text()) {
		alert("You must Get a Client first!");
	}
	$.post('/training-stop', {
		name: training_name,
		client: $('#client-name').text()
	}).done(function(d){
		getAllTrainings();
	});
}

function addDiss(){
	DisseasesTam = $('add-new-diss p').size() + 1;
	var addDss = $('#add-new-diss');
    $('<p><input type="text" id="diss-new-' + DisseasesTam + '" size="40" name="p-new-' + DisseasesTam +'" value="" placeholder="New Dissease" /><a class="btn btn-default" href="" id="rm-diss">Remove</a> </p>').appendTo(addDss);
    DisseasesTam ++;
    return false;
}

function addInjuries(){
	InjuriesTam = $('add-new-inj p').size() + 1;
	var addInj = $('#add-new-inj');
    $('<p><input class="form-control-inline" type="text" id="inj-new-' + InjuriesTam + '" size="40" name="p-new-' + InjuriesTam +'" value="" placeholder="New Dissease" /><a class="btn btn-default" href="" id="rm-inj">Remove</a> </p>').appendTo(addInj);
    InjuriesTam ++;
    return false;
}

function editClientData(){
 	$('#editting-buttons').show();
 	$('#normal-buttons').hide();
 	$('#data-client').hide();
 	$('#data-trainer-edit').show();
}

function cancelEditClientData(){
	var trainerName = $('#client-name').text();
	var trainerEmail = $('#client-email').text();
	$('#client-email-edit').val(trainerEmail);
	$('#client-name-edit').val(trainerName);
	$('#editting-buttons').hide();
 	$('#normal-buttons').show();
 	$('#data-client').show();
 	$('#data-client-edit').hide();
}

$(function() {
	$("#rm-diss").on("click", function() {
		$(this).parent('p').remove();
	});
	
	$("#rm-inj").on("click", function() {
		$(this).parent('p').remove();
	});
});