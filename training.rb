require 'sinatra'
require 'json'
require 'sqlite3'

db_filepath = ENV['CICLOSANOS_DB']
puts "DATABASE FILEPATH", db_filepath

db = SQLite3::Database.new(db_filepath)
# Creating table (comment if unnecessary):
=begin
puts db.execute("drop table trainings")
puts db.execute("drop table client_training")
puts db.execute("create table trainings (name varchar(50), popularity int)")
puts db.execute("create table client_training (client_name varchar(50), training_name varchar(50))")
# puts db.execute("select * from trainings")
=end

get '/' do
  '<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>'
end

get '/trainings' do
  rows = db.execute("SELECT * FROM trainings")
  rows.to_json
end

get '/trainings/clients' do
  rows = db.execute("SELECT * FROM client_training")
  rows.to_json
end

delete '/trainings/clients/:name' do
  name = params[:name]
  db.execute("DELETE FROM client_training WHERE client_name=?", name)
end

post '/trainings/clients' do
  data = JSON.parse(request.body.read)
  name = data["name"]
  rows = db.execute("SELECT popularity FROM trainings WHERE name=?", name)
  if rows.count == 0
    halt "{\"error\": \"training #{name} doesn't exist\"}"
  end
  popularity = rows[0][0]
  popularity += 1
  db.execute("UPDATE trainings SET popularity=? WHERE name=?", popularity, name)
  client = data["client"]
  rows = db.execute("INSERT INTO client_training (training_name, client_name) VALUES (?, ?)", name, client)
  rows.to_json
end

post '/trainings' do
  data = JSON.parse(request.body.read)
  db.execute("INSERT INTO trainings (name, popularity) VALUES (?, ?)", data["name"], 0)
end

delete '/trainings/:name' do
  name = params[:name]
  db.execute("DELETE FROM client_training WHERE training_name=?", name)
  db.execute("DELETE FROM trainings WHERE name=?", name)
end