
/**
 * FormValidatorsSkeletonInterface.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */
    package org.example.validators.formvalidators;
    /**
     *  FormValidatorsSkeletonInterface java skeleton interface for the axisService
     */
    public interface FormValidatorsSkeletonInterface {
     
         
        /**
         * Auto generated method signature
         * 
                                    * @param validateClientForm
         */

        
                public org.example.validators.formvalidators.ValidateClientFormResponse validateClientForm
                (
                  org.example.validators.formvalidators.ValidateClientForm validateClientForm
                 )
            ;
        
         
        /**
         * Auto generated method signature
         * 
                                    * @param validateTrainerForm
         */

        
                public org.example.validators.formvalidators.ValidateTrainerFormResponse validateTrainerForm
                (
                  org.example.validators.formvalidators.ValidateTrainerForm validateTrainerForm
                 )
            ;
        
         }
    