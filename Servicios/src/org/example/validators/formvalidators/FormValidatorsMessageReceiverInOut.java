
/**
 * FormValidatorsMessageReceiverInOut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */
        package org.example.validators.formvalidators;

        /**
        *  FormValidatorsMessageReceiverInOut message receiver
        */

        public class FormValidatorsMessageReceiverInOut extends org.apache.axis2.receivers.AbstractInOutMessageReceiver{


        public void invokeBusinessLogic(org.apache.axis2.context.MessageContext msgContext, org.apache.axis2.context.MessageContext newMsgContext)
        throws org.apache.axis2.AxisFault{

        try {

        // get the implementation class for the Web Service
        Object obj = getTheImplementationObject(msgContext);

        FormValidatorsSkeletonInterface skel = (FormValidatorsSkeletonInterface)obj;
        //Out Envelop
        org.apache.axiom.soap.SOAPEnvelope envelope = null;
        //Find the axisOperation that has been set by the Dispatch phase.
        org.apache.axis2.description.AxisOperation op = msgContext.getOperationContext().getAxisOperation();
        if (op == null) {
        throw new org.apache.axis2.AxisFault("Operation is not located, if this is doclit style the SOAP-ACTION should specified via the SOAP Action to use the RawXMLProvider");
        }

        java.lang.String methodName;
        if((op.getName() != null) && ((methodName = org.apache.axis2.util.JavaUtils.xmlNameToJavaIdentifier(op.getName().getLocalPart())) != null)){


        

            if("validateClientForm".equals(methodName)){
                
                org.example.validators.formvalidators.ValidateClientFormResponse validateClientFormResponse5 = null;
	                        org.example.validators.formvalidators.ValidateClientForm wrappedParam =
                                                             (org.example.validators.formvalidators.ValidateClientForm)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    org.example.validators.formvalidators.ValidateClientForm.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               validateClientFormResponse5 =
                                                   
                                                   
                                                         skel.validateClientForm(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), validateClientFormResponse5, false, new javax.xml.namespace.QName("http://validators.example.org/FormValidators/",
                                                    "validateClientForm"));
                                    } else 

            if("validateTrainerForm".equals(methodName)){
                
                org.example.validators.formvalidators.ValidateTrainerFormResponse validateTrainerFormResponse7 = null;
	                        org.example.validators.formvalidators.ValidateTrainerForm wrappedParam =
                                                             (org.example.validators.formvalidators.ValidateTrainerForm)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    org.example.validators.formvalidators.ValidateTrainerForm.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               validateTrainerFormResponse7 =
                                                   
                                                   
                                                         skel.validateTrainerForm(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), validateTrainerFormResponse7, false, new javax.xml.namespace.QName("http://validators.example.org/FormValidators/",
                                                    "validateTrainerForm"));
                                    
            } else {
              throw new java.lang.RuntimeException("method not found");
            }
        

        newMsgContext.setEnvelope(envelope);
        }
        }
        catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
        }
        
        //
            private  org.apache.axiom.om.OMElement  toOM(org.example.validators.formvalidators.ValidateClientForm param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(org.example.validators.formvalidators.ValidateClientForm.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(org.example.validators.formvalidators.ValidateClientFormResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(org.example.validators.formvalidators.ValidateClientFormResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(org.example.validators.formvalidators.ValidateTrainerForm param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(org.example.validators.formvalidators.ValidateTrainerForm.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(org.example.validators.formvalidators.ValidateTrainerFormResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(org.example.validators.formvalidators.ValidateTrainerFormResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, org.example.validators.formvalidators.ValidateClientFormResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(org.example.validators.formvalidators.ValidateClientFormResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private org.example.validators.formvalidators.ValidateClientFormResponse wrapValidateClientForm(){
                                org.example.validators.formvalidators.ValidateClientFormResponse wrappedElement = new org.example.validators.formvalidators.ValidateClientFormResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, org.example.validators.formvalidators.ValidateTrainerFormResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(org.example.validators.formvalidators.ValidateTrainerFormResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private org.example.validators.formvalidators.ValidateTrainerFormResponse wrapValidateTrainerForm(){
                                org.example.validators.formvalidators.ValidateTrainerFormResponse wrappedElement = new org.example.validators.formvalidators.ValidateTrainerFormResponse();
                                return wrappedElement;
                         }
                    


        /**
        *  get the default envelope
        */
        private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory){
        return factory.getDefaultEnvelope();
        }


        private  java.lang.Object fromOM(
        org.apache.axiom.om.OMElement param,
        java.lang.Class type,
        java.util.Map extraNamespaces) throws org.apache.axis2.AxisFault{

        try {
        
                if (org.example.validators.formvalidators.ValidateClientForm.class.equals(type)){
                
                           return org.example.validators.formvalidators.ValidateClientForm.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (org.example.validators.formvalidators.ValidateClientFormResponse.class.equals(type)){
                
                           return org.example.validators.formvalidators.ValidateClientFormResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (org.example.validators.formvalidators.ValidateTrainerForm.class.equals(type)){
                
                           return org.example.validators.formvalidators.ValidateTrainerForm.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (org.example.validators.formvalidators.ValidateTrainerFormResponse.class.equals(type)){
                
                           return org.example.validators.formvalidators.ValidateTrainerFormResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
        } catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
           return null;
        }



    

        /**
        *  A utility method that copies the namepaces from the SOAPEnvelope
        */
        private java.util.Map getEnvelopeNamespaces(org.apache.axiom.soap.SOAPEnvelope env){
        java.util.Map returnMap = new java.util.HashMap();
        java.util.Iterator namespaceIterator = env.getAllDeclaredNamespaces();
        while (namespaceIterator.hasNext()) {
        org.apache.axiom.om.OMNamespace ns = (org.apache.axiom.om.OMNamespace) namespaceIterator.next();
        returnMap.put(ns.getPrefix(),ns.getNamespaceURI());
        }
        return returnMap;
        }

        private org.apache.axis2.AxisFault createAxisFault(java.lang.Exception e) {
        org.apache.axis2.AxisFault f;
        Throwable cause = e.getCause();
        if (cause != null) {
            f = new org.apache.axis2.AxisFault(e.getMessage(), cause);
        } else {
            f = new org.apache.axis2.AxisFault(e.getMessage());
        }

        return f;
    }

        }//end of class
    