
/**
 * FormValidatorsSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */
    package org.example.validators.formvalidators;

import java.util.Calendar;

import org.example.www.Client;
import org.example.www.Trainer;

    public class FormValidatorsSkeleton implements FormValidatorsSkeletonInterface{
        
         
    	public ValidateClientFormResponse validateClientForm(ValidateClientForm req) {
    	    String errors = "";
    	    ValidateClientFormResponse ret = new ValidateClientFormResponse();
    	    ret.setValid(true);

    	    Client client = req.getClient();

    	    if (client.getName().matches("(.*)[^a-zA-Z ](.*)")) {
    	      ret.setValid(false);
    	      errors += "El nombre no puede contener caracteres no alfabéticos\n";
    	    }

    	    if (!client.getEmail().matches("(.+)@(.+)[.](.+)")) {
    	      ret.setValid(false);
    	      errors += "El email tiene un formato incorrecto\n";
    	    }

    	    if (client.getBday().after(Calendar.getInstance().getTime())) {
    	      ret.setValid(false);
    	      errors += "La fecha es incorrecta\n";
    	    }

    	    ret.setError(errors);
    	    return ret;
    	  }
    	
    	public ValidateTrainerFormResponse validateTrainerForm(
    		      ValidateTrainerForm req) {
    		    String errors = "";
    		    ValidateTrainerFormResponse ret = new ValidateTrainerFormResponse();
    		    ret.setValid(true);

    		    Trainer trainer = req.getTrainer();

    		    if (trainer.getName().matches("(.*)[^a-zA-Z ](.*)")) {
    		      ret.setValid(false);
    		      errors += "El nombre no puede contener caracteres no alfabéticos\n";
    		    }

    		    if (!trainer.getEmail().matches("(.+)@(.+)[.](.+)")) {
    		      ret.setValid(false);
    		      errors += "El email tiene un formato incorrecto\n";
    		    }

    		    ret.setError(errors);
    		    return ret;
    		  }

     
    }
    