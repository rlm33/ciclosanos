package org.example.www.util;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class SQLUtil {
	// Singleton
	private SQLUtil() {
	}

	public static String[] arrayFromResultSet(ResultSet rs, String column)
			throws SQLException {
		String[] ret;
		ArrayList<String> ary = new ArrayList<String>();

		while (rs.next()) {
			ary.add(rs.getString(column));
		}
		ret = new String[ary.size()];
		for (int i = 0; i < ret.length; i++) {
			ret[i] = ary.get(i);
		}

		return ret;
	}

	public static String insertIntoJoinTable(String table, String mainColumn,
			String mainValue, String valuesColumn, String[] values) {
		String sql = "INSERT INTO " + table + " (" + mainColumn + ", "
				+ valuesColumn + ") VALUES ";
		sql += valuesForJoinTable(mainValue, values);
		return sql;
	}

	public static String valuesForJoinTable(String target, String[] ary) {
		String sql = "";
		int i;

		for (i = 0; i < ary.length - 1; i++) {
			sql += "(" + "'" + target + "'," + "'" + ary[i] + "'" + "),";
		}
		if (ary.length > 0) {
			sql += "(" + "'" + target + "'," + "'" + ary[i] + "'" + ")";
		}

		return sql;
	}
}
