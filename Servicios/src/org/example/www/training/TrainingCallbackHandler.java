
/**
 * TrainingCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */

    package org.example.www.training;

    /**
     *  TrainingCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class TrainingCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public TrainingCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public TrainingCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for updateTraining method
            * override this method for handling normal response from updateTraining operation
            */
           public void receiveResultupdateTraining(
                    org.example.www.UpdateTrainingResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from updateTraining operation
           */
            public void receiveErrorupdateTraining(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for deleteTraining method
            * override this method for handling normal response from deleteTraining operation
            */
           public void receiveResultdeleteTraining(
                    org.example.www.DeleteTrainingResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from deleteTraining operation
           */
            public void receiveErrordeleteTraining(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for incrementPopularity method
            * override this method for handling normal response from incrementPopularity operation
            */
           public void receiveResultincrementPopularity(
                    org.example.www.IncrementPopularityResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from incrementPopularity operation
           */
            public void receiveErrorincrementPopularity(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for createTraining method
            * override this method for handling normal response from createTraining operation
            */
           public void receiveResultcreateTraining(
                    org.example.www.CreateTrainingResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from createTraining operation
           */
            public void receiveErrorcreateTraining(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getTraining method
            * override this method for handling normal response from getTraining operation
            */
           public void receiveResultgetTraining(
                    org.example.www.GetTrainingResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getTraining operation
           */
            public void receiveErrorgetTraining(java.lang.Exception e) {
            }
                


    }
    