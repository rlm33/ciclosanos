
/**
 * TrainingSkeletonInterface.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */
    package org.example.www.training;
    /**
     *  TrainingSkeletonInterface java skeleton interface for the axisService
     */
    public interface TrainingSkeletonInterface {
     
         
        /**
         * Auto generated method signature
         * 
                                    * @param updateTraining
         */

        
                public org.example.www.UpdateTrainingResponse updateTraining
                (
                  org.example.www.UpdateTraining updateTraining
                 )
            ;
        
         
        /**
         * Auto generated method signature
         * 
                                    * @param deleteTraining
         */

        
                public org.example.www.DeleteTrainingResponse deleteTraining
                (
                  org.example.www.DeleteTraining deleteTraining
                 )
            ;
        
         
        /**
         * Auto generated method signature
         * 
                                    * @param incrementPopularity
         */

        
                public org.example.www.IncrementPopularityResponse incrementPopularity
                (
                  org.example.www.IncrementPopularity incrementPopularity
                 )
            ;
        
         
        /**
         * Auto generated method signature
         * 
                                    * @param createTraining
         */

        
                public org.example.www.CreateTrainingResponse createTraining
                (
                  org.example.www.CreateTraining createTraining
                 )
            ;
        
         
        /**
         * Auto generated method signature
         * 
                                    * @param getTraining
         */

        
                public org.example.www.GetTrainingResponse getTraining
                (
                  org.example.www.GetTraining getTraining
                 )
            ;
        
         }
    