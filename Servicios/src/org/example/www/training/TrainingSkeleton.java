
/**
 * TrainingSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */
    package org.example.www.training;
    /**
     *  TrainingSkeleton java skeleton for the axisService
     */
    public class TrainingSkeleton implements TrainingSkeletonInterface{
        
         
        /**
         * Auto generated method signature
         * 
                                     * @param updateTraining0 
             * @return updateTrainingResponse1 
         */
        
                 public org.example.www.UpdateTrainingResponse updateTraining
                  (
                  org.example.www.UpdateTraining updateTraining0
                  )
            {
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#updateTraining");
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param deleteTraining2 
             * @return deleteTrainingResponse3 
         */
        
                 public org.example.www.DeleteTrainingResponse deleteTraining
                  (
                  org.example.www.DeleteTraining deleteTraining2
                  )
            {
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#deleteTraining");
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param incrementPopularity4 
             * @return incrementPopularityResponse5 
         */
        
                 public org.example.www.IncrementPopularityResponse incrementPopularity
                  (
                  org.example.www.IncrementPopularity incrementPopularity4
                  )
            {
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#incrementPopularity");
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param createTraining6 
             * @return createTrainingResponse7 
         */
        
                 public org.example.www.CreateTrainingResponse createTraining
                  (
                  org.example.www.CreateTraining createTraining6
                  )
            {
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#createTraining");
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param getTraining8 
             * @return getTrainingResponse9 
         */
        
                 public org.example.www.GetTrainingResponse getTraining
                  (
                  org.example.www.GetTraining getTraining8
                  )
            {
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#getTraining");
        }
     
    }
    