package org.example.www.repositories;

import org.example.www.Diet;

public interface IDietRepository {
	public Diet GetByName(String clientName);

	public boolean Create(Diet target);
	
	public void Close();
}
