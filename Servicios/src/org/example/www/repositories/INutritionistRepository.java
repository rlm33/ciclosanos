package org.example.www.repositories;

import org.example.www.Nutritionist;

public interface INutritionistRepository {
	public Nutritionist GetByEmail(String nutritionistEmail);

	public boolean Create(Nutritionist target);
	
	public boolean Update(Nutritionist target);
	
	public boolean DeleteByEmail(String nutritionistEmail);
	
	public void Close();
}
