package org.example.www.repositories;

import org.example.www.Food;

public interface IFoodRepository {
	public Food GetByName(String foodName);

	public boolean Create(Food target);
	
	public boolean Update(Food target);
	
	public boolean DeleteByName(String foodName);
	
	public void Close();
}
