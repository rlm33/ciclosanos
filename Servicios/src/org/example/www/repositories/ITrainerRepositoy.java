package org.example.www.repositories;

import org.example.www.Trainer; 

public interface ITrainerRepositoy {
	public Trainer GetByEmail(String trainerEmail);

	public boolean Create(Trainer target);
	
	public boolean Update(Trainer target);
	
	public boolean DeleteByEmail(String trainerEmail);
	
	public void Close();
}
