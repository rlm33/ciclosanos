package org.example.www.repositories;

import org.example.www.Tracing;

public interface ITracingRepository{
	public Tracing GetById(int tracingId);

	public boolean Create(Tracing target);
	
	public boolean Update(Tracing target, int tracingId);
	
	public boolean DeleteById(int tracingId);
	
	public void Close();
}
