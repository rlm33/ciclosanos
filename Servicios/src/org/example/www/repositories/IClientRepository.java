package org.example.www.repositories;

import org.example.www.Client;

public interface IClientRepository {
	public Client GetByName(String clientName);

	public boolean Create(Client target);
	
	public boolean Update(Client target);
	
	public boolean DeleteByName(String clientName);
	
	public void Close();
}
