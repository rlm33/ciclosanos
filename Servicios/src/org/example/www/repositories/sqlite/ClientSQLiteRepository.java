package org.example.www.repositories.sqlite;

import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.example.www.Client;
import org.example.www.repositories.IClientRepository;
import org.example.www.util.SQLUtil;

public class ClientSQLiteRepository extends SQLiteRepository implements
		IClientRepository {

	public ClientSQLiteRepository(String databaseFilePath) {
		super(databaseFilePath);
	}

	public ClientSQLiteRepository() {
		super();
	}

	public Client GetByName(String clientName) {
		Client ret = null;
		try {
			Statement statement = this.conn.createStatement();
			statement.setQueryTimeout(30); // set timeout to 30 sec.

			ResultSet rs = statement
					.executeQuery("SELECT * FROM clients WHERE name='"
							+ clientName + "'");
			if (rs.next()) {
				ret = new Client();
				ret.setName(rs.getString("name"));
				ret.setEmail(rs.getString("email"));
				String strbday = rs.getString("bday");
				Date bday = Date.valueOf(strbday);
				ret.setBday(bday);

				rs = statement
						.executeQuery("SELECT diseaseName FROM clients_diseases WHERE clientName ='"
								+ clientName + "'");
				String[] diseases = SQLUtil.arrayFromResultSet(rs,
						"diseaseName");
				ret.setDiseases(diseases);

				rs = statement
						.executeQuery("SELECT injuryName FROM clients_injuries WHERE clientName='"
								+ clientName + "'");

				String[] injuries = SQLUtil
						.arrayFromResultSet(rs, "injuryName");
				ret.setInjuries(injuries);
			}
		} catch (SQLException e) {
			// if the error message is "out of memory",
			// it probably means no database file is found
			System.err.println(e);
		}
		return ret;
	}

	public boolean Create(Client target) {
		boolean done = false;
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		try {
			Statement statement = this.conn.createStatement();
			statement.setQueryTimeout(30); // set timeout to 30 sec.

		
			ResultSet rs = statement
					.executeQuery("SELECT name FROM clients WHERE name='"
							+ target.getName() + "'");
	
			if (rs.next()) {
				System.out.println("si next");
				return false;
			}

			String sql = "INSERT INTO clients (name, email, bday) VALUES ";
			sql += "(" + "'" + target.getName() + "'," + "'"
					+ target.getEmail() + "'," + "'"
					+ dateFormat.format(target.getBday()) + "'" + ")";
	
			statement.executeUpdate(sql);

			if (target.getDiseases() != null && target.getDiseases().length > 0) {
				insertDiseases(target.getName(), target.getDiseases());
			}
			
			if (target.getInjuries() != null && target.getInjuries().length > 0) {
				insertInjuries(target.getName(), target.getInjuries());
			}

			done = true;
		} catch (SQLException e) {
			System.err.println(e);
		}
		return done;
	}

	private void insertDiseases(String clientName, String[] diseases) {
		try {
			Statement statement = this.conn.createStatement();
			statement.setQueryTimeout(30); // set timeout to 30 sec.

			String sql = SQLUtil.insertIntoJoinTable("clients_diseases",
					"clientName", clientName, "diseaseName", diseases);

			statement.executeUpdate(sql);
		} catch (SQLException e) {
			System.err.println(e);
		}
	}

	private void insertInjuries(String clientName, String[] injuries) {
		try {
			Statement statement = this.conn.createStatement();
			statement.setQueryTimeout(30); // set timeout to 30 sec.

			String sql = SQLUtil.insertIntoJoinTable("clients_injuries",
					"clientName", clientName, "injuryName", injuries);

			statement.executeUpdate(sql);
		} catch (SQLException e) {
			System.err.println(e);
		}
	}

	public boolean Update(Client target) {
		boolean ret;

		DeleteByName(target.getName());
		ret = Create(target);

		return ret;
	}

	public boolean DeleteByName(String clientName) {
		boolean ret = false;
		try {
			Statement statement = this.conn.createStatement();
			statement.setQueryTimeout(30); // set timeout to 30 sec.

			String sql = "DELETE FROM clients WHERE name='" + clientName + "'";
			statement.executeUpdate(sql);
			sql = "DELETE FROM clients_diseases WHERE clientName='"
					+ clientName + "'";
			statement.executeUpdate(sql);
			sql = "DELETE FROM clients_injuries WHERE clientName='"
					+ clientName + "'";
			statement.executeUpdate(sql);

			ret = true;
		} catch (SQLException e) {
			System.err.println(e);
		}
		return ret;
	}

}
