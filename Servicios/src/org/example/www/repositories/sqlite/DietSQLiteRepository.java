package org.example.www.repositories.sqlite;

import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import org.example.www.Diet;
import org.example.www.Dish;
import org.example.www.DishesOfDay;
import org.example.www.DishesOfWeek;
import org.example.www.Food;
import org.example.www.repositories.IDietRepository;

public class DietSQLiteRepository extends SQLiteRepository implements IDietRepository {

	public DietSQLiteRepository(String databaseFilePath) {
		super(databaseFilePath);
	}

	public DietSQLiteRepository() {
		super();
	}
	
	public Diet GetByName(String dietName) {
		Diet ret = null;
		String[] myStringArray = new String[]{"a","b","c"};
		Dish d = null;
		Dish dbreakfast = null;
		Dish[] db = null;
		try {
			Statement statement = this.conn.createStatement();
			statement.setQueryTimeout(30); // set timeout to 30 sec.

			ResultSet rs = statement.executeQuery("SELECT name, benefitsSummary, popularity FROM diets WHERE name = '" + dietName + "'");
			//ResultSet rsBreakfast = statement.executeQuery("SELECT breakfasts.name FROM dishesOfDays and breakfasts WHERE dishesOfDays.id = breakfasts.idDishesOfDay and dishesOfDays.dietName = '" + dietName + "'");
			//ResultSet rsLunch = statement.executeQuery("SELECT lunches.name FROM dishesOfDays and lunches WHERE dishesOfDays.id = lunches.idDishesOfDays and dishesOfDays.dietName = '" + dietName + "'");
			//ResultSet rsDinner = statement.executeQuery("SELECT dinner.name FROM dishesOfDay and dinners WHERE dishesOfDays.id = dinners.idDishesOfDays and dishesOfDays.dietName = '" + dietName + "'");
			//ResultSet rsDish = statement.executeQuery("SELECT dishes.name, dishes, FROM dishes WHERE dishes.dietName = '" + dietName + "'");
			//ResultSet rsDishToFood = statement.executeQuery("SELECT dishesToFoods.fooName, FROM dishes and dishesToFoods WHERE dishes.name = dishsesToFoods.dishName and dishes.dietName = '" + dietName + "'");*/
			
			if(rs != null) {
				DishesOfWeek dow = new DishesOfWeek();
				DishesOfDay dod = new DishesOfDay();
				ret = new Diet();
				//d.setPictures(myStringArray);
				
				//DIETS
				
				ret.setName(rs.getString("name"));
				ret.setBenefitsSummary(rs.getString("benefitsSummary"));
				ret.setPopularity(rs.getInt("popularity"));
				
				//DIESTSTOFOOD
				rs = statement.executeQuery("SELECT FoodName FROM dietsToFoods WHERE dietName = '" + dietName + "'");
				if(rs != null){
					ArrayList<String> ary = new ArrayList<String>();
					while (rs.next()) {
						ary.add(rs.getString("FoodName"));
					}
					
					String[] foods = new String[ary.size()];
					
					for(int i = 0; i < ary.size(); i++){
						foods[i] = ary.get(i);
					}
				
					ret.setFoods(foods);
					
					//DISHESOFDAY
					rs = statement.executeQuery("SELECT dayId, dietName FROM dishesOfDays WHERE dietName = '" + dietName + "'");
					if(rs.next()){
						dow.setMonday(setResultOfDay("L", dietName));
						dow.setTuesday(setResultOfDay("M", dietName));
						dow.setWednesday(setResultOfDay("X", dietName));
						dow.setThursday(setResultOfDay("J", dietName));
						dow.setFriday(setResultOfDay("V", dietName));
						dow.setSaturday(setResultOfDay("S", dietName));
						dow.setSunday(setResultOfDay("D", dietName));
	
						ret.setDishesByDay(dow);
						//rs = statement.executeQuery("SELECT dishes.name, dishes, FROM dishes WHERE dishes.dietName = '" + dietName + "'");
						//d.setFoods(foods);	
					}
				}
			}
		} catch (SQLException e) {
			// if the error message is "out of memory",
			// it probably means no database file is found
			System.err.println(e.getMessage());
		}
		return ret;
	}

	public boolean Create(Diet target) {
		boolean done = false;
		String[] dayString = new String[]{"L","M","X","J","V","S","D"};
		
		try {
			Statement statement = this.conn.createStatement();
			statement.setQueryTimeout(30); // set timeout to 30 sec.
			
			String sql = "INSERT INTO diets (name, benefitsSummary, popularity) VALUES ";
			sql += "(" 
					+ "'" + target.getName() + "'," 
					+ "'" + target.getBenefitsSummary() + "'," 
					+ "'" + target.getPopularity() + "'" 
				+ ")";
			statement.executeUpdate(sql);
			
			for(int i = 0; i < target.getFoods().length; i++){
				sql = "INSERT INTO dietsToFoods (FoodName, dietName) VALUES ";
				sql += "(" 
						+ "'" + target.getFoods()[i] + "'," 
						+ "'" + target.getName() + "'" 
					+ ")";
				statement.executeUpdate(sql);
			}
			
			for(int i = 0; i < 7; i++){
				sql = "INSERT INTO dishesOfDays (dayId, dietName) VALUES ";
				sql += "("
						+ "'" + dayString[i] + "'," 
						+ "'" + target.getName() + "'"
					+ ")";
				statement.executeUpdate(sql);
			}
			
			for(int i = 0; i < target.getDishesByDay().getMonday().getBreakfast().length; i++){
				sql = "INSERT INTO breakfasts (eatId, name, idDishesOfDay, dietName) VALUES ";
				sql += "("
						+ "'B'," 
						+ "'" + target.getDishesByDay().getMonday().getBreakfast()[i].getName() + "',"
						+ "'L',"
						+ "'" + target.getName() + "'"
					+ ")";
				statement.executeUpdate(sql);
			}
			
			for(int i = 0; i < target.getDishesByDay().getTuesday().getBreakfast().length; i++){
				sql = "INSERT INTO breakfasts (eatId, name, idDishesOfDay, dietName) VALUES ";
				sql += "("
						+ "'B'," 
						+ "'" + target.getDishesByDay().getTuesday().getBreakfast()[i].getName() + "',"
						+ "'M',"
						+ "'" + target.getName() + "'"
					+ ")";
				statement.executeUpdate(sql);
			}
			
			for(int i = 0; i < target.getDishesByDay().getWednesday().getBreakfast().length; i++){
				sql = "INSERT INTO breakfasts (eatId, name, idDishesOfDay, dietName) VALUES ";
				sql += "("
						+ "'B'," 
						+ "'" + target.getDishesByDay().getWednesday().getBreakfast()[i].getName() + "',"
						+ "'X',"
						+ "'" + target.getName() + "'"
					+ ")";
				statement.executeUpdate(sql);
			}
			
			for(int i = 0; i < target.getDishesByDay().getThursday().getBreakfast().length; i++){
				sql = "INSERT INTO breakfasts (eatId, name, idDishesOfDay, dietName) VALUES ";
				sql += "("
						+ "'B'," 
						+ "'" + target.getDishesByDay().getThursday().getBreakfast()[i].getName() + "',"
						+ "'J',"
						+ "'" + target.getName() + "'"
					+ ")";
				statement.executeUpdate(sql);
			}
			
			for(int i = 0; i < target.getDishesByDay().getFriday().getBreakfast().length; i++){
				sql = "INSERT INTO breakfasts (eatId, name, idDishesOfDay, dietName) VALUES ";
				sql += "("
						+ "'B'," 
						+ "'" + target.getDishesByDay().getFriday().getBreakfast()[i].getName() + "',"
						+ "'V',"
						+ "'" + target.getName() + "'"
					+ ")";
				statement.executeUpdate(sql);
			}
			
			for(int i = 0; i < target.getDishesByDay().getSaturday().getBreakfast().length; i++){
				sql = "INSERT INTO breakfasts (eatId, name, idDishesOfDay, dietName) VALUES ";
				sql += "("
						+ "'B'," 
						+ "'" + target.getDishesByDay().getSaturday().getBreakfast()[i].getName() + "',"
						+ "'S',"
						+ "'" + target.getName() + "'"
					+ ")";
				statement.executeUpdate(sql);
			}
			
			for(int i = 0; i < target.getDishesByDay().getSunday().getBreakfast().length; i++){
				sql = "INSERT INTO breakfasts (eatId, name, idDishesOfDay, dietName) VALUES ";
				sql += "("
						+ "'B'," 
						+ "'" + target.getDishesByDay().getSunday().getBreakfast()[i].getName() + "',"
						+ "'D',"
						+ "'" + target.getName() + "'"
					+ ")";
				statement.executeUpdate(sql);
			}
			
			
			done = true;
		} catch (SQLException e) {
			System.err.println(e);
		}
		return done;
	}
	
	private DishesOfDay setResultOfDay(String day, String dietName){
		DishesOfDay dod = null;
		String[] myStringArray = new String[]{"a","b","c"};
		try {
			Statement statement = this.conn.createStatement();
			statement.setQueryTimeout(30); // set timeout to 30 sec.
			ResultSet rs = statement.executeQuery("SELECT name FROM breakfasts WHERE idDishesOfDay = '"+ day + "' and breakfasts.dietName = '" + dietName + "'");					
			if(rs != null){							
				Dish dbreakfast = new Dish();
				dbreakfast.setFoods(myStringArray);
				dbreakfast.setName(rs.getString("name"));
				
				Dish[] db = new Dish[]{dbreakfast};
				dod = new DishesOfDay();
				
				dod.setBreakfast(db);
				dod.setDinner(db);
				dod.setLunch(db);
			}
		}catch (SQLException e) {
			// if the error message is "out of memory",
			// it probably means no database file is found
			System.err.println(e.getMessage());
		}
		return dod;
	}

}
