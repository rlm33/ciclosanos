package org.example.www.repositories.sqlite;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class SQLiteRepository {
	protected Connection conn;

	private void Initialize(String databaseFilePath) {
		this.conn = null;
		try {
			Class.forName("org.sqlite.JDBC");

			this.conn = DriverManager.getConnection("jdbc:sqlite:"
					+ databaseFilePath);
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
	}

	public SQLiteRepository(String databaseFilePath) {
		Initialize(databaseFilePath);
	}

	public SQLiteRepository() {
		Initialize(SQLiteDatabaseConfig.fileNamePath);
	}

	public void ExecuteUpdate(String sql) {
		try {
			Statement statement = this.conn.createStatement();
			statement.setQueryTimeout(30); // set timeout to 30 sec.

			statement.executeUpdate(sql);
		} catch (SQLException e) {
			// if the error message is "out of memory",
			// it probably means no database file is found
			System.err.println(e.getMessage());
		}
	}

	public void Close() {
		try {
			if (this.conn != null) {
				this.conn.close();
			}
		} catch (SQLException e) {
			System.err.println(e);
		}
	}
}
