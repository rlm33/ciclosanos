package org.example.www.repositories.sqlite;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.example.www.Trainer;
import org.example.www.repositories.ITrainerRepositoy;

public class TrainerSQLiteRepository extends SQLiteRepository implements
		ITrainerRepositoy {
	
	public TrainerSQLiteRepository(String databaseFilePath) {
		super(databaseFilePath);
	}

	public TrainerSQLiteRepository() {
		super();
	}

	public boolean Create(Trainer target) {
		boolean done = false;
		try {
			Statement statement = this.conn.createStatement();
			statement.setQueryTimeout(30); // set timeout to 30 sec.

			// Anyadimos nutricionista
			String sql = "INSERT INTO trainers (name, email) VALUES ";
			sql += "(" + "'" + target.getName() + "'," + "'"
					+ target.getEmail() + "'" + ")";
			statement.executeUpdate(sql);
			done = true;
		} catch (SQLException e) {
			System.err.println(e);
		}
		return done;
	}

	public boolean Update(Trainer target) {
		
		boolean ret = false;
		
		DeleteByEmail(target.getEmail());
		ret = Create(target);
		
		return ret;
	}

	public Trainer GetByEmail(String trainerEmail) {
		Trainer ret = null;
		try {
			Statement statement = this.conn.createStatement();
			statement.setQueryTimeout(30); // set timeout to 30 sec.

			ResultSet rs = statement
					.executeQuery("SELECT * FROM trainers WHERE email='"
							+ trainerEmail + "'");
			if (rs.next()) {
				ret = new Trainer();
				ret.setName(rs.getString("name"));
				ret.setEmail(rs.getString("email"));
			}
		} catch (SQLException e) {
			System.err.println(e);
		}
		return ret;
	}

	public boolean DeleteByEmail(String trainerEmail) {
		
		boolean ret = false;
		
		try {
			Statement statement = this.conn.createStatement();
			statement.setQueryTimeout(30); // set timeout to 30 sec
			statement.executeUpdate("DELETE FROM trainers WHERE email='"+trainerEmail+"'");
			ret = true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		
		return ret;
	}

}
