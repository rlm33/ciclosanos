package org.example.www.repositories.sqlite;

public class SQLiteDatabaseInit {

	public static void main(String[] args) {
		String databaseFileNamePath;

		if (args.length > 0) {
			databaseFileNamePath = args[0];
		} else {
			databaseFileNamePath = SQLiteDatabaseConfig.fileNamePath;
		}
		
		System.out.println("[CREATING FILE] " + databaseFileNamePath);
		SQLiteRepository repo = new SQLiteRepository(databaseFileNamePath);
		
		System.out.println("[CREATE TABLE clients]");
		repo.ExecuteUpdate("DROP TABLE IF EXISTS clients;");
		repo.ExecuteUpdate("CREATE TABLE clients (name varchar(50), email varchar(50), bday char(11));");
		
		System.out.println("[CREATE TABLE clients_diseases]");
		repo.ExecuteUpdate("DROP TABLE IF EXISTS clients_diseases;");
		repo.ExecuteUpdate("CREATE TABLE clients_diseases (clientName varchar(50), diseaseName varchar(50));");
		
		System.out.println("[CREATE TABLE clients_injuries]");
		repo.ExecuteUpdate("DROP TABLE IF EXISTS clients_injuries;");
		repo.ExecuteUpdate("CREATE TABLE clients_injuries (clientName varchar(50), injuryName varchar(50));");
		
		System.out.println("[CREATE TABLE foods]");
		repo.ExecuteUpdate("DROP TABLE IF EXISTS foods;");
		repo.ExecuteUpdate("CREATE TABLE foods (name varchar(50));");
		
		System.out.println("[CREATE TABLE beneficialForDiseases]");
		repo.ExecuteUpdate("DROP TABLE IF EXISTS beneficialForDiseases;");
		repo.ExecuteUpdate("CREATE TABLE beneficialForDiseases (name varchar(50), foodName varchar(50));");
		
		System.out.println("[CREATE TABLE hazardousForDiseases]");
		repo.ExecuteUpdate("DROP TABLE IF EXISTS hazardousForDiseases;");
		repo.ExecuteUpdate("CREATE TABLE hazardousForDiseases (name varchar(50), foodName varchar(50));");
		
		System.out.println("[CREATE TABLE nutritionists]");
		repo.ExecuteUpdate("DROP TABLE IF EXISTS nutritionists;");
		repo.ExecuteUpdate("CREATE TABLE nutritionists (name varchar(50), email varchar(50));");
		
		System.out.println("[CREATE TABLE diets]");
		repo.ExecuteUpdate("DROP TABLE IF EXISTS diets;");
		repo.ExecuteUpdate("CREATE TABLE diets (name varchar(50), benefitsSummary varchar(250), popularity int);");
		
		System.out.println("[CREATE TABLE dietsToFoods]");
		repo.ExecuteUpdate("DROP TABLE IF EXISTS dietsToFoods;");
		repo.ExecuteUpdate("CREATE TABLE dietsToFoods (FoodName varchar(50), dietName varchar(50));");
		
		System.out.println("[CREATE TABLE trainers]");
		repo.ExecuteUpdate("DROP TABLE IF EXISTS trainers;");
		repo.ExecuteUpdate("CREATE TABLE trainers (name varchar(50), email varchar(50));");
		
		System.out.println("[CREATE TABLE tracings]");
		repo.ExecuteUpdate("DROP TABLE IF EXISTS tracings;");
		repo.ExecuteUpdate("CREATE TABLE tracings (id INTEGER PRIMARY KEY AUTOINCREMENT, clientName varchar(50), type varchar(50), target varchar(50), active boolean);");
		
		System.out.println("[CREATE TABLE tracings_data]");
		repo.ExecuteUpdate("DROP TABLE IF EXISTS tracings_data;");
		repo.ExecuteUpdate("CREATE TABLE tracings_data (tracing_id INTEGER, data varchar(255));");

		System.out.println("[CREATE TABLE dishesOfDays]");
		repo.ExecuteUpdate("DROP TABLE IF EXISTS dishesOfDays;");
		repo.ExecuteUpdate("CREATE TABLE dishesOfDays (dayId char, dietName varchar(50));");
		
		System.out.println("[CREATE TABLE breakfasts]");
		repo.ExecuteUpdate("DROP TABLE IF EXISTS breakfasts;");
		repo.ExecuteUpdate("CREATE TABLE breakfasts (eatId char, name varchar(50), idDishesOfDay char, dietName varchar(50));");
		
		/*
		System.out.println("[CREATE TABLE lunches]");
		repo.ExecuteUpdate("DROP TABLE IF EXISTS lunches;");
		repo.ExecuteUpdate("CREATE TABLE lunches (eatId char, name varchar(50), idDishesOfDay INTEGER);");
		
		System.out.println("[CREATE TABLE dinners]");
		repo.ExecuteUpdate("DROP TABLE IF EXISTS dinners;");
		repo.ExecuteUpdate("CREATE TABLE dinners (eatId char, name varchar(50), idDishesOfDay INTEGER, dietName varchar(50));");
		
		System.out.println("[CREATE TABLE dishes]");
		repo.ExecuteUpdate("DROP TABLE IF EXISTS dishes;");
		repo.ExecuteUpdate("CREATE TABLE dishes (name varchar(50), pictures text, eatName char);");
		
		System.out.println("[CREATE TABLE dishesToFoods]");
		repo.ExecuteUpdate("DROP TABLE IF EXISTS dishesToFoods;");
		repo.ExecuteUpdate("CREATE TABLE dishesToFoods (dishName varchar(50), foodName varchar(50));");*/
		
		System.out.println("[FINISHED SCHEMA]");
	}

}
