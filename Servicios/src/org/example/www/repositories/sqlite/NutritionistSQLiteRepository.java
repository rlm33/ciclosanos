package org.example.www.repositories.sqlite;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.example.www.Nutritionist;
import org.example.www.repositories.INutritionistRepository;

public class NutritionistSQLiteRepository extends SQLiteRepository implements
		INutritionistRepository {

	public NutritionistSQLiteRepository(String databaseFilePath) {
		super(databaseFilePath);
	}

	public NutritionistSQLiteRepository() {
		super();
	}

	public Nutritionist GetByEmail(String nutritionistEmail) {
		Nutritionist ret = null;
		try {
			Statement statement = this.conn.createStatement();
			statement.setQueryTimeout(30); // set timeout to 30 sec.

			ResultSet rs = statement
					.executeQuery("SELECT * FROM nutritionists WHERE email='"
							+ nutritionistEmail + "'");
			if (rs.next()) {
				ret = new Nutritionist();
				ret.setName(rs.getString("name"));
				ret.setEmail(rs.getString("email"));
			}
		} catch (SQLException e) {
			System.err.println(e);
		}
		return ret;
	}

	public boolean Create(Nutritionist target) {
		boolean done = false;
		try {
			Statement statement = this.conn.createStatement();
			statement.setQueryTimeout(30); // set timeout to 30 sec.

			// Anyadimos nutricionista
			String sql = "INSERT INTO nutritionists (name, email) VALUES ";
			sql += "(" + "'" + target.getName() + "'," + "'"
					+ target.getEmail() + "'" + ")";
			statement.executeUpdate(sql);
			done = true;
		} catch (SQLException e) {
			System.err.println(e);
		}
		return done;
	}

	public boolean Update(Nutritionist target) {
		
		boolean ret = false;
		
		DeleteByEmail(target.getEmail());
		ret = Create(target);
		
		return ret;
	}

	public boolean DeleteByEmail(String nutritionistEmail) {
		
		boolean ret = false;
		
		try {
			Statement statement = this.conn.createStatement();
			statement.setQueryTimeout(30); // set timeout to 30 sec
			statement.executeUpdate("DELETE FROM nutritionists WHERE email='"+nutritionistEmail+"'");
			ret = true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		
		return ret;
	}
}
