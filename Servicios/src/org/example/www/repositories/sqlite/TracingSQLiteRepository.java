package org.example.www.repositories.sqlite;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.example.www.Nutritionist;
import org.example.www.Tracing;
import org.example.www.TracingTypes;
import org.example.www.repositories.ITracingRepository;
import org.example.www.util.SQLUtil;

public class TracingSQLiteRepository extends SQLiteRepository implements
		ITracingRepository {

	public TracingSQLiteRepository(String databaseFilePath) {
		super(databaseFilePath);
	}

	public TracingSQLiteRepository() {
		super();
	}

	public Tracing GetById(int tracingId) {
		Tracing ret = null;
		try {
			Statement statement = this.conn.createStatement();
			statement.setQueryTimeout(30); // set timeout to 30 sec.

			ResultSet rs = statement
					.executeQuery("SELECT * FROM tracings WHERE id='"
							+ tracingId + "'");
			if (rs.next()) {
				ret = new Tracing();
				ret.setClientName(rs.getString("clientName"));
				ret.setTarget(rs.getString("target"));
				ret.setActive(rs.getBoolean("active"));
				TracingTypes tt = TracingTypes.Factory.fromValue(rs
						.getString("type"));
				ret.setType(tt);

				// Tabla tracings_data
				rs = statement
						.executeQuery("SELECT data FROM tracings_data WHERE tracing_id='"
								+ tracingId + "'");
				String[] data = SQLUtil.arrayFromResultSet(rs, "data");
				ret.setData(data);
			}
		} catch (SQLException e) {
			System.err.println(e);
		}
		return ret;
	}

	public boolean Create(Tracing target) {
		boolean done = false;
		try {
			Statement statement = this.conn.createStatement();
			statement.setQueryTimeout(30); // set timeout to 30 sec.
			
			//Anyadimos tracing
			String sql = "INSERT INTO tracings (clientName,type,target,active) VALUES ";
			sql += "(" 
					+ "'" + target.getClientName() + "',"
					+ "'" + target.getType() + "',"
					+ "'" + target.getTarget() + "',"
					+ "'" + target.getActive() + "'"
				+ ")";
			statement.executeUpdate(sql);
			
			//Anyadimos los beneficialForDiseases (Es una tabla aparte)
			sql = "INSERT INTO tracings_data (tracing_id, data) VALUES ";
			int i;
			for(i=0;i<target.getData().length-1;i++){
				sql += "(" 
						+ "'" + GetTracingId() + "'," //TODO
						+ "'" + target.getData()[i] + "'"
					+ "),";
			}
			if (target.getData().length > 0) {
				sql += "(" 
						+ "'" + GetTracingId() + "'," //TODO
						+ "'" + target.getData()[i] + "'"
					+ ")";
			}
			statement.executeUpdate(sql);
			
			done = true;
		} catch (SQLException e) {
			System.err.println(e);
		}
		return done;
	}

	public boolean DeleteById(int tracingId) {
		boolean ret = false;
		try {
			Statement statement = this.conn.createStatement();
			statement.setQueryTimeout(30); // set timeout to 30 sec
			
			statement.executeUpdate("DELETE FROM tracings WHERE id='"+tracingId+"'");
			statement.executeUpdate("DELETE FROM tracings_data WHERE tracing_id='"+tracingId+"'");
		
			ret = true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ret;
	}
	
	//Obtener Id dado un Tracing
	private String GetTracingId(){
		String res="";

		try {
			Statement statement = this.conn.createStatement();
			statement.setQueryTimeout(30); // set timeout to 30 sec.
			
			ResultSet rs = statement
					.executeQuery("SELECT max(id) as id FROM tracings");
			res = rs.getString("id");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return res;
	}

	public boolean Update(Tracing target, int tracingId) {
		
		boolean ret = false;
		
		DeleteById(tracingId);
		ret = Create(target);
		
		return ret;
	}

}
