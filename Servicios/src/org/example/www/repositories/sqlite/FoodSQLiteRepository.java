package org.example.www.repositories.sqlite;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.example.www.Food;
import org.example.www.repositories.IFoodRepository;

public class FoodSQLiteRepository extends SQLiteRepository implements
		IFoodRepository {

	public FoodSQLiteRepository(String databaseFilePath) {
		super(databaseFilePath);
	}

	public FoodSQLiteRepository() {
		super();
	}

	public Food GetByName(String foodName) {

		ArrayList<String> arrayBeneficial = new ArrayList<String>();
		ArrayList<String> arrayHazard = new ArrayList<String>();

		Food ret = null;
		try {
			Statement statement = this.conn.createStatement();
			statement.setQueryTimeout(30); // set timeout to 30 sec.

			ResultSet rs = statement
					.executeQuery("SELECT * FROM foods WHERE name='" + foodName
							+ "'");
			if (rs.next()) {
				ret = new Food();
				ret.setName(rs.getString("name"));

				//Tabla beneficialForDiseases
				rs = statement
						.executeQuery("SELECT * FROM beneficialForDiseases WHERE foodName='"
								+ foodName + "'");
				while (rs.next()) {
					arrayBeneficial.add(rs.getString("name"));
				}
				String[] listBeneficial = new String[arrayBeneficial.size()];
				for (int i = 0; i < arrayBeneficial.size(); i++) {
					listBeneficial[i] = arrayBeneficial.get(i);
				}
				ret.setBeneficialForDiseases(listBeneficial);
				
				//Tabla hazardousForDiseases
				rs = statement
						.executeQuery("SELECT * FROM hazardousForDiseases WHERE foodName='"
								+ foodName + "'");
				while (rs.next()) {
					arrayHazard.add(rs.getString("name"));
				}
				String[] listHazard = new String[arrayHazard.size()];
				for (int i = 0; i < arrayHazard.size(); i++) {
					listHazard[i] = arrayHazard.get(i);
				}
				ret.setHazardousForDiseases(listHazard);
				
			}
		} catch (SQLException e) {
			// if the error message is "out of memory",
			// it probably means no database file is found
			System.err.println(e.getMessage());
		}
		return ret;
	}

	public boolean Create(Food target) {
		boolean done = false;
		try {
			Statement statement = this.conn.createStatement();
			statement.setQueryTimeout(30); // set timeout to 30 sec.
			
			//Anyadimos la comida
			String sql = "INSERT INTO foods (name) VALUES ";
			sql += "(" 
					+ "'" + target.getName() + "'" 
				+ ")";
			statement.executeUpdate(sql);
			
			//Anyadimos los beneficialForDiseases (Es una tabla aparte)
			sql = "INSERT INTO beneficialForDiseases (name, foodName) VALUES ";
			int i;
			for(i=0;i<target.getBeneficialForDiseases().length-1;i++){
				sql += "(" 
						+ "'" + target.getBeneficialForDiseases()[i] + "',"
						+ "'" + target.getName() + "'"
					+ "),";
			}
			if (target.getBeneficialForDiseases().length > 0) {
				sql += "(" 
						+ "'" + target.getBeneficialForDiseases()[i] + "',"
						+ "'" + target.getName() + "'"
					+ ")";
			}
			statement.executeUpdate(sql);
			
			//Anyadimos los hazardousForDiseases (Es una tabla aparte)
			sql = "INSERT INTO hazardousForDiseases (name, foodName) VALUES ";
			for(i=0;i<target.getHazardousForDiseases().length-1;i++){
				sql += "(" 
						+ "'" + target.getHazardousForDiseases()[i] + "',"
						+ "'" + target.getName() + "'"
					+ "),";
			}
			if (target.getHazardousForDiseases().length > 0) {
				sql += "(" 
						+ "'" + target.getHazardousForDiseases()[i] + "',"
						+ "'" + target.getName() + "'"
					+ ")";
			}
			statement.executeUpdate(sql);
			
			done = true;
		} catch (SQLException e) {
			System.err.println(e);
		}
		return done;
	}

	public boolean DeleteByName(String foodName) {
		boolean ret = false;
		try {
			Statement statement = this.conn.createStatement();
			statement.setQueryTimeout(30); // set timeout to 30 sec
			
			statement.executeUpdate("DELETE FROM foods WHERE name='"+foodName+"'");
			statement.executeUpdate("DELETE FROM beneficialForDiseases WHERE foodName='"+foodName+"'");
			statement.executeUpdate("DELETE FROM hazardousForDiseases WHERE foodName='"+foodName+"'");
		
			ret = true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ret;
	}

	public boolean Update(Food target) {
		
		boolean ret = false;
		
		DeleteByName(target.getName());
		ret = Create(target);
		
		return ret;
	}

}
