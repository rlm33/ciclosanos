
/**
 * ClientSkeletonInterface.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */
    package org.example.www.client;
    /**
     *  ClientSkeletonInterface java skeleton interface for the axisService
     */
    public interface ClientSkeletonInterface {
     
         
        /**
         * Auto generated method signature
         * 
                                    * @param getClient
         */

        
                public org.example.www.GetClientResponse getClient
                (
                  org.example.www.GetClient getClient
                 )
            ;
        
         
        /**
         * Auto generated method signature
         * 
                                    * @param deleteClient
         */

        
                public org.example.www.DeleteClientResponse deleteClient
                (
                  org.example.www.DeleteClient deleteClient
                 )
            ;
        
         
        /**
         * Auto generated method signature
         * 
                                    * @param updateClient
         */

        
                public org.example.www.UpdateClientResponse updateClient
                (
                  org.example.www.UpdateClient updateClient
                 )
            ;
        
         
        /**
         * Auto generated method signature
         * 
                                    * @param createClient
         */

        
                public org.example.www.CreateClientResponse createClient
                (
                  org.example.www.CreateClient createClient
                 )
            ;
        
         }
    