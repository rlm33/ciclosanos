/**
 * ClientSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */
package org.example.www.client;

import org.example.www.Client;
import org.example.www.CreateClient;
import org.example.www.CreateClientResponse;
import org.example.www.DeleteClient;
import org.example.www.DeleteClientResponse;
import org.example.www.GetClient;
import org.example.www.GetClientResponse;
import org.example.www.UpdateClient;
import org.example.www.UpdateClientResponse;
import org.example.www.repositories.IClientRepository;
import org.example.www.repositories.sqlite.ClientSQLiteRepository;

/**
 * ClientSkeleton java skeleton for the axisService
 */
public class ClientSkeleton implements ClientSkeletonInterface {

	private final IClientRepository clientRepository = new ClientSQLiteRepository();

	/**
	 * 
	 * @param getClientRequest
	 * @return getClientResponse
	 */

	public GetClientResponse getClient(GetClient getClientRequest) {
		GetClientResponse ret = new GetClientResponse();
		ret.setExists(false);
		String clientName = getClientRequest.getName();
		Client target = this.clientRepository.GetByName(clientName);
		ret.setClient(target);
		if (target != null) {
			ret.setExists(true);
		}

		this.clientRepository.Close();

		return ret;
	}

	/**
	 * 
	 * @param deleteClientRequest
	 * @return deleteClientResponse
	 */

	public DeleteClientResponse deleteClient(DeleteClient deleteClientRequest) {
		DeleteClientResponse ret = new DeleteClientResponse();

		boolean done = this.clientRepository.DeleteByName(deleteClientRequest
				.getName());
		ret.setDone(done);
		// TODO: Manage string error
		ret.setError("");

		return ret;
	}

	/**
	 * 
	 * @param updateClientRequest
	 * @return updateClientResponse
	 */

	public UpdateClientResponse updateClient(UpdateClient updateClientRequest) {
		UpdateClientResponse ret = new UpdateClientResponse();

		boolean done = this.clientRepository.Update(updateClientRequest
				.getClient());
		ret.setDone(done);
		// TODO: Manage string error
		ret.setError("");

		return ret;
	}

	/**
	 * 
	 * @param createClientRequest
	 * @return createClientResponse
	 */

	public CreateClientResponse createClient(CreateClient createClientRequest) {
		CreateClientResponse ret = new CreateClientResponse();
		Client target = createClientRequest.getClient();
		
		boolean done = this.clientRepository.Create(target);
	
		ret.setDone(done);
		
		// TODO: Manage string error
		ret.setError("");
		

		return ret;
	}


}
