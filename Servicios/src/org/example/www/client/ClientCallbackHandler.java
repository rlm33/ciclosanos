
/**
 * ClientCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */

    package org.example.www.client;

    /**
     *  ClientCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class ClientCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public ClientCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public ClientCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for getClient method
            * override this method for handling normal response from getClient operation
            */
           public void receiveResultgetClient(
                    org.example.www.GetClientResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getClient operation
           */
            public void receiveErrorgetClient(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for deleteClient method
            * override this method for handling normal response from deleteClient operation
            */
           public void receiveResultdeleteClient(
                    org.example.www.DeleteClientResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from deleteClient operation
           */
            public void receiveErrordeleteClient(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for updateClient method
            * override this method for handling normal response from updateClient operation
            */
           public void receiveResultupdateClient(
                    org.example.www.UpdateClientResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from updateClient operation
           */
            public void receiveErrorupdateClient(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for createClient method
            * override this method for handling normal response from createClient operation
            */
           public void receiveResultcreateClient(
                    org.example.www.CreateClientResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from createClient operation
           */
            public void receiveErrorcreateClient(java.lang.Exception e) {
            }
                


    }
    