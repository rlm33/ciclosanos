
/**
 * FormValidatorsSkeletonInterface.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */
    package org.example.www.formvalidators;
    /**
     *  FormValidatorsSkeletonInterface java skeleton interface for the axisService
     */
    public interface FormValidatorsSkeletonInterface {
     
         
        /**
         * Auto generated method signature
         * 
                                    * @param validateTrainerForm
         */

        
                public org.example.www.ValidateTrainerFormResponse validateTrainerForm
                (
                  org.example.www.ValidateTrainerForm validateTrainerForm
                 )
            ;
        
         
        /**
         * Auto generated method signature
         * 
                                    * @param validateClientForm
         */

        
                public org.example.www.ValidateClientFormResponse validateClientForm
                (
                  org.example.www.ValidateClientForm validateClientForm
                 )
            ;
        
         
        /**
         * Auto generated method signature
         * 
                                    * @param validateNutritionistForm
         */

        
                public org.example.www.ValidateNutritionistFormResponse validateNutritionistForm
                (
                  org.example.www.ValidateNutritionistForm validateNutritionistForm
                 )
            ;
        
         }
    