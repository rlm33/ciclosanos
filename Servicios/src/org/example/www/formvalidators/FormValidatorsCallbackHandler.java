
/**
 * FormValidatorsCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */

    package org.example.www.formvalidators;

    /**
     *  FormValidatorsCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class FormValidatorsCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public FormValidatorsCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public FormValidatorsCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for validateTrainerForm method
            * override this method for handling normal response from validateTrainerForm operation
            */
           public void receiveResultvalidateTrainerForm(
                    org.example.www.ValidateTrainerFormResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from validateTrainerForm operation
           */
            public void receiveErrorvalidateTrainerForm(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for validateClientForm method
            * override this method for handling normal response from validateClientForm operation
            */
           public void receiveResultvalidateClientForm(
                    org.example.www.ValidateClientFormResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from validateClientForm operation
           */
            public void receiveErrorvalidateClientForm(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for validateNutritionistForm method
            * override this method for handling normal response from validateNutritionistForm operation
            */
           public void receiveResultvalidateNutritionistForm(
                    org.example.www.ValidateNutritionistFormResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from validateNutritionistForm operation
           */
            public void receiveErrorvalidateNutritionistForm(java.lang.Exception e) {
            }
                


    }
    