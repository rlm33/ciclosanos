/**
 * FormValidatorsSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */
package org.example.www.formvalidators;

import java.util.Calendar;
import java.util.Date;

import org.example.www.Client;
import org.example.www.Nutritionist;
import org.example.www.Trainer;
import org.example.www.ValidateClientForm;
import org.example.www.ValidateClientFormResponse;
import org.example.www.ValidateNutritionistForm;
import org.example.www.ValidateNutritionistFormResponse;
import org.example.www.ValidateTrainerForm;
import org.example.www.ValidateTrainerFormResponse;

/**
 * FormValidatorsSkeleton java skeleton for the axisService
 */
public class FormValidatorsSkeleton implements FormValidatorsSkeletonInterface {

	public ValidateTrainerFormResponse validateTrainerForm(
			ValidateTrainerForm req) {
		String errors = "";
		ValidateTrainerFormResponse ret = new ValidateTrainerFormResponse();
		ret.setValid(true);

		Trainer trainer = req.getTrainer();

		if (trainer.getName().matches("(.*)[^a-zA-Z ](.*)")) {
			ret.setValid(false);
			errors += "El nombre no puede contener caracteres no alfabéticos\n";
		}

		if (!trainer.getEmail().matches("(.+)@(.+)[.](.+)")) {
			ret.setValid(false);
			errors += "El email tiene un formato incorrecto\n";
		}

		ret.setError(errors);
		return ret;
	}

	public ValidateClientFormResponse validateClientForm(ValidateClientForm req) {
		String errors = "";
		ValidateClientFormResponse ret = new ValidateClientFormResponse();
		ret.setValid(true);

		Client client = req.getClient();

		if (client.getName().matches("(.*)[^a-zA-Z ](.*)")) {
			ret.setValid(false);
			errors += "El nombre no puede contener caracteres no alfabéticos\n";
		}

		if (!client.getEmail().matches("(.+)@(.+)[.](.+)")) {
			ret.setValid(false);
			errors += "El email tiene un formato incorrecto\n";
		}

		if (client.getBday().after(Calendar.getInstance().getTime())) {
			ret.setValid(false);
			errors += "La fecha es incorrecta\n";
		}

		ret.setError(errors);
		return ret;
	}

	public ValidateNutritionistFormResponse validateNutritionistForm(
			ValidateNutritionistForm req) {
		String errors = "";
		ValidateNutritionistFormResponse ret = new ValidateNutritionistFormResponse();
		ret.setValid(true);
		
		Nutritionist nutritionist = req.getNutritionist();
		
		if (nutritionist.getName().matches("(.*)[^a-zA-Z ](.*)")) {
			ret.setValid(false);
			errors += "El nombre no puede contener caracteres no alfabéticos\n";
		}

		if (!nutritionist.getEmail().matches("(.+)@(.+)[.](.+)")) {
			ret.setValid(false);
			errors += "El email tiene un formato incorrecto\n";
		}
		
		ret.setError(errors);
		return ret;
	}

}
