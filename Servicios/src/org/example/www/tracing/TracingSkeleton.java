/**
 * TracingSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */
package org.example.www.tracing;

import org.example.www.CreateFoodResponse;
import org.example.www.CreateTracing;
import org.example.www.CreateTracingResponse;
import org.example.www.DeleteFoodResponse;
import org.example.www.DeleteTracing;
import org.example.www.DeleteTracingResponse;
import org.example.www.Food;
import org.example.www.GetFoodResponse;
import org.example.www.GetTracing;
import org.example.www.GetTracingResponse;
import org.example.www.Tracing;
import org.example.www.UpdateFoodResponse;
import org.example.www.UpdateTracing;
import org.example.www.UpdateTracingResponse;
import org.example.www.repositories.IFoodRepository;
import org.example.www.repositories.ITracingRepository;
import org.example.www.repositories.sqlite.FoodSQLiteRepository;
import org.example.www.repositories.sqlite.TracingSQLiteRepository;

/**
 * TracingSkeleton java skeleton for the axisService
 */
public class TracingSkeleton implements TracingSkeletonInterface {

	private final ITracingRepository tracingRepository = new TracingSQLiteRepository();
	
	/**
	 * Auto generated method signature
	 * 
	 * @param updateTracing0
	 * @return updateTracingResponse1
	 */

	public UpdateTracingResponse updateTracing(UpdateTracing req) {
		UpdateTracingResponse ret = new UpdateTracingResponse();
		
		boolean done = this.tracingRepository.Update(req.getTracing(), req.getIdTracing());
		ret.setDone(done);
		// TODO: Manage string error
		ret.setError("");
		
		return ret;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param createTracing2
	 * @return createTracingResponse3
	 */

	public CreateTracingResponse createTracing(CreateTracing req) {
		CreateTracingResponse ret = new CreateTracingResponse();
		Tracing target = req.getTracing();
		boolean done = this.tracingRepository.Create(target);
		ret.setDone(done);
		// TODO: Manage string error
		ret.setError("");
		
		this.tracingRepository.Close();
		
		return ret;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param getTracing4
	 * @return getTracingResponse5
	 */

	public GetTracingResponse getTracing(GetTracing req) {
		GetTracingResponse ret = new GetTracingResponse();
		int tracingId = req.getId();
		ret.setExists(false);
		Tracing target = this.tracingRepository.GetById(tracingId);
		if (target != null) {
			ret.setTracing(target);
			ret.setExists(true);
		}
		
		this.tracingRepository.Close();
		
		return ret;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param deleteTracing6
	 * @return deleteTracingResponse7
	 */

	public DeleteTracingResponse deleteTracing(DeleteTracing req) {
		DeleteTracingResponse ret = new DeleteTracingResponse();
		
		boolean done = this.tracingRepository.DeleteById(req.getId());
		ret.setDone(done);
		// TODO: Manage string error
		ret.setError("");
		
		return ret;
	}

}
