
/**
 * TracingCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */

    package org.example.www.tracing;

    /**
     *  TracingCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class TracingCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public TracingCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public TracingCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for updateTracing method
            * override this method for handling normal response from updateTracing operation
            */
           public void receiveResultupdateTracing(
                    org.example.www.UpdateTracingResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from updateTracing operation
           */
            public void receiveErrorupdateTracing(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for createTracing method
            * override this method for handling normal response from createTracing operation
            */
           public void receiveResultcreateTracing(
                    org.example.www.CreateTracingResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from createTracing operation
           */
            public void receiveErrorcreateTracing(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getTracing method
            * override this method for handling normal response from getTracing operation
            */
           public void receiveResultgetTracing(
                    org.example.www.GetTracingResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getTracing operation
           */
            public void receiveErrorgetTracing(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for deleteTracing method
            * override this method for handling normal response from deleteTracing operation
            */
           public void receiveResultdeleteTracing(
                    org.example.www.DeleteTracingResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from deleteTracing operation
           */
            public void receiveErrordeleteTracing(java.lang.Exception e) {
            }
                


    }
    