
/**
 * TracingSkeletonInterface.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */
    package org.example.www.tracing;
    /**
     *  TracingSkeletonInterface java skeleton interface for the axisService
     */
    public interface TracingSkeletonInterface {
     
         
        /**
         * Auto generated method signature
         * 
                                    * @param updateTracing
         */

        
                public org.example.www.UpdateTracingResponse updateTracing
                (
                  org.example.www.UpdateTracing updateTracing
                 )
            ;
        
         
        /**
         * Auto generated method signature
         * 
                                    * @param createTracing
         */

        
                public org.example.www.CreateTracingResponse createTracing
                (
                  org.example.www.CreateTracing createTracing
                 )
            ;
        
         
        /**
         * Auto generated method signature
         * 
                                    * @param getTracing
         */

        
                public org.example.www.GetTracingResponse getTracing
                (
                  org.example.www.GetTracing getTracing
                 )
            ;
        
         
        /**
         * Auto generated method signature
         * 
                                    * @param deleteTracing
         */

        
                public org.example.www.DeleteTracingResponse deleteTracing
                (
                  org.example.www.DeleteTracing deleteTracing
                 )
            ;
        
         }
    