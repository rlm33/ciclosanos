/**
 * DietSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */
package org.example.www.diet;

import org.example.www.*;
import org.example.www.repositories.IDietRepository;
import org.example.www.repositories.sqlite.DietSQLiteRepository;

/**
 * DietSkeleton java skeleton for the axisService
 */
public class DietSkeleton implements DietSkeletonInterface {

	private final IDietRepository dietRepository = new DietSQLiteRepository();

	/**
	 * Auto generated method signature
	 * 
	 * @param createDiet0
	 * @return createDietResponse1
	 */

	public CreateDietResponse createDiet(CreateDiet createDietRequest) {
		CreateDietResponse ret = new CreateDietResponse();
		Diet target = createDietRequest.getDiet();
		boolean done = this.dietRepository.Create(target);
		ret.setDone(done);
		 //TODO: Manage string error
		ret.setError("");
		
		this.dietRepository.Close();
		
		//ret.setDone(true);
		
		return ret;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param deleteDiet2
	 * @return deleteDietResponse3
	 */

	public org.example.www.DeleteDietResponse deleteDiet(
			org.example.www.DeleteDiet deleteDiet2) {
		// TODO : fill this with the necessary business logic
		throw new java.lang.UnsupportedOperationException("Please implement "
				+ this.getClass().getName() + "#deleteDiet");
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param incrementPopularity4
	 * @return incrementPopularityResponse5
	 */

	public org.example.www.IncrementPopularityResponse incrementPopularity(
			org.example.www.IncrementPopularity incrementPopularity4) {
		// TODO : fill this with the necessary business logic
		throw new java.lang.UnsupportedOperationException("Please implement "
				+ this.getClass().getName() + "#incrementPopularity");
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param getDiet6
	 * @return getDietResponse7
	 */

	public org.example.www.GetDietResponse getDiet(GetDiet getDietRequest) {
		GetDietResponse ret = new GetDietResponse();
		ret.setExists(false);
		String dietName = getDietRequest.getName();
		Diet target = this.dietRepository.GetByName(dietName);
		ret.setDiet(target);
		if (target != null) {
			ret.setExists(true);
		}
		
		this.dietRepository.Close();
		
		return ret;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param updateDiet8
	 * @return updateDietResponse9
	 */

	public org.example.www.UpdateDietResponse updateDiet(
			org.example.www.UpdateDiet updateDiet8) {
		// TODO : fill this with the necessary business logic
		throw new java.lang.UnsupportedOperationException("Please implement "
				+ this.getClass().getName() + "#updateDiet");
	}

}
