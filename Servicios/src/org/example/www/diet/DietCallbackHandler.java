
/**
 * DietCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */

    package org.example.www.diet;

    /**
     *  DietCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class DietCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public DietCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public DietCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for createDiet method
            * override this method for handling normal response from createDiet operation
            */
           public void receiveResultcreateDiet(
                    org.example.www.CreateDietResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from createDiet operation
           */
            public void receiveErrorcreateDiet(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for deleteDiet method
            * override this method for handling normal response from deleteDiet operation
            */
           public void receiveResultdeleteDiet(
                    org.example.www.DeleteDietResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from deleteDiet operation
           */
            public void receiveErrordeleteDiet(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for incrementPopularity method
            * override this method for handling normal response from incrementPopularity operation
            */
           public void receiveResultincrementPopularity(
                    org.example.www.IncrementPopularityResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from incrementPopularity operation
           */
            public void receiveErrorincrementPopularity(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDiet method
            * override this method for handling normal response from getDiet operation
            */
           public void receiveResultgetDiet(
                    org.example.www.GetDietResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDiet operation
           */
            public void receiveErrorgetDiet(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for updateDiet method
            * override this method for handling normal response from updateDiet operation
            */
           public void receiveResultupdateDiet(
                    org.example.www.UpdateDietResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from updateDiet operation
           */
            public void receiveErrorupdateDiet(java.lang.Exception e) {
            }
                


    }
    