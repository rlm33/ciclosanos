
/**
 * DietSkeletonInterface.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */
    package org.example.www.diet;
    /**
     *  DietSkeletonInterface java skeleton interface for the axisService
     */
    public interface DietSkeletonInterface {
     
         
        /**
         * Auto generated method signature
         * 
                                    * @param createDiet
         */

        
                public org.example.www.CreateDietResponse createDiet
                (
                  org.example.www.CreateDiet createDiet
                 )
            ;
        
         
        /**
         * Auto generated method signature
         * 
                                    * @param deleteDiet
         */

        
                public org.example.www.DeleteDietResponse deleteDiet
                (
                  org.example.www.DeleteDiet deleteDiet
                 )
            ;
        
         
        /**
         * Auto generated method signature
         * 
                                    * @param incrementPopularity
         */

        
                public org.example.www.IncrementPopularityResponse incrementPopularity
                (
                  org.example.www.IncrementPopularity incrementPopularity
                 )
            ;
        
         
        /**
         * Auto generated method signature
         * 
                                    * @param getDiet
         */

        
                public org.example.www.GetDietResponse getDiet
                (
                  org.example.www.GetDiet getDiet
                 )
            ;
        
         
        /**
         * Auto generated method signature
         * 
                                    * @param updateDiet
         */

        
                public org.example.www.UpdateDietResponse updateDiet
                (
                  org.example.www.UpdateDiet updateDiet
                 )
            ;
        
         }
    