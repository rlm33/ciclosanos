
/**
 * TrainerSkeletonInterface.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */
    package org.example.www.trainer;
    /**
     *  TrainerSkeletonInterface java skeleton interface for the axisService
     */
    public interface TrainerSkeletonInterface {
     
         
        /**
         * Auto generated method signature
         * 
                                    * @param updateTrainer
         */

        
                public org.example.www.UpdateTrainerResponse updateTrainer
                (
                  org.example.www.UpdateTrainer updateTrainer
                 )
            ;
        
         
        /**
         * Auto generated method signature
         * 
                                    * @param getTrainer
         */

        
                public org.example.www.GetTrainerResponse getTrainer
                (
                  org.example.www.GetTrainer getTrainer
                 )
            ;
        
         
        /**
         * Auto generated method signature
         * 
                                    * @param deleteTrainer
         */

        
                public org.example.www.DeleteTrainerResponse deleteTrainer
                (
                  org.example.www.DeleteTrainer deleteTrainer
                 )
            ;
        
         
        /**
         * Auto generated method signature
         * 
                                    * @param createTrainer
         */

        
                public org.example.www.CreateTrainerResponse createTrainer
                (
                  org.example.www.CreateTrainer createTrainer
                 )
            ;
        
         }
    