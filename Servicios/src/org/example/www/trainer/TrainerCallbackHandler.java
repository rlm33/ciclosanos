
/**
 * TrainerCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */

    package org.example.www.trainer;

    /**
     *  TrainerCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class TrainerCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public TrainerCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public TrainerCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for updateTrainer method
            * override this method for handling normal response from updateTrainer operation
            */
           public void receiveResultupdateTrainer(
                    org.example.www.UpdateTrainerResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from updateTrainer operation
           */
            public void receiveErrorupdateTrainer(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getTrainer method
            * override this method for handling normal response from getTrainer operation
            */
           public void receiveResultgetTrainer(
                    org.example.www.GetTrainerResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getTrainer operation
           */
            public void receiveErrorgetTrainer(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for deleteTrainer method
            * override this method for handling normal response from deleteTrainer operation
            */
           public void receiveResultdeleteTrainer(
                    org.example.www.DeleteTrainerResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from deleteTrainer operation
           */
            public void receiveErrordeleteTrainer(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for createTrainer method
            * override this method for handling normal response from createTrainer operation
            */
           public void receiveResultcreateTrainer(
                    org.example.www.CreateTrainerResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from createTrainer operation
           */
            public void receiveErrorcreateTrainer(java.lang.Exception e) {
            }
                


    }
    