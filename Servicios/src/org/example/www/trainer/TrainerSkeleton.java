/**
 * TrainerSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */
package org.example.www.trainer;

import org.example.www.CreateTrainer;
import org.example.www.CreateTrainerResponse;
import org.example.www.DeleteTrainer;
import org.example.www.DeleteTrainerResponse;
import org.example.www.GetTrainer;
import org.example.www.GetTrainerResponse;
import org.example.www.Trainer;
import org.example.www.UpdateTrainer;
import org.example.www.UpdateTrainerResponse;
import org.example.www.repositories.ITrainerRepositoy;
import org.example.www.repositories.sqlite.TrainerSQLiteRepository;

/**
 * TrainerSkeleton java skeleton for the axisService
 */
public class TrainerSkeleton implements TrainerSkeletonInterface {
	
	private final ITrainerRepositoy trainerRepository = new TrainerSQLiteRepository();

	/**
	 * Auto generated method signature
	 * 
	 * @param updateTrainer0
	 * @return updateTrainerResponse1
	 */

	public UpdateTrainerResponse updateTrainer(UpdateTrainer req) {
		UpdateTrainerResponse ret = new UpdateTrainerResponse();
		
		boolean done = this.trainerRepository.Update(req.getTrainer());
		ret.setDone(done);
		// TODO: Manage string error
		ret.setError("");
		
		return ret;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param getTrainer2
	 * @return getTrainerResponse3
	 */

	public GetTrainerResponse getTrainer(GetTrainer req) {
		GetTrainerResponse ret = new GetTrainerResponse();
		String trainerName = req.getEmail();
		ret.setDone(false);
		Trainer target = this.trainerRepository.GetByEmail(trainerName);
		if (target != null) {
			ret.setTrainer(target);
			ret.setDone(true);
		}
		
		this.trainerRepository.Close();
		
		return ret;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param deleteTrainer4
	 * @return deleteTrainerResponse5
	 */

	public DeleteTrainerResponse deleteTrainer(DeleteTrainer req) {
		DeleteTrainerResponse ret = new DeleteTrainerResponse();
		
		boolean done = this.trainerRepository.DeleteByEmail(req.getEmail());
		ret.setDone(done);
		// TODO: Manage string error
		ret.setError("");
		
		return ret;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param createTrainer6
	 * @return createTrainerResponse7
	 */

	public CreateTrainerResponse createTrainer(CreateTrainer req) {
		CreateTrainerResponse ret = new CreateTrainerResponse();
		Trainer target = req.getTrainer();
		boolean done = this.trainerRepository.Create(target);
		ret.setDone(done);
		// TODO: Manage string error
		ret.setError("");
		
		this.trainerRepository.Close();
		
		return ret;
	}

}
