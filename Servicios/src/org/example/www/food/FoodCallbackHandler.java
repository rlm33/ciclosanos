
/**
 * FoodCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */

    package org.example.www.food;

    /**
     *  FoodCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class FoodCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public FoodCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public FoodCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for getFood method
            * override this method for handling normal response from getFood operation
            */
           public void receiveResultgetFood(
                    org.example.www.GetFoodResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getFood operation
           */
            public void receiveErrorgetFood(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for updateFood method
            * override this method for handling normal response from updateFood operation
            */
           public void receiveResultupdateFood(
                    org.example.www.UpdateFoodResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from updateFood operation
           */
            public void receiveErrorupdateFood(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for deleteFood method
            * override this method for handling normal response from deleteFood operation
            */
           public void receiveResultdeleteFood(
                    org.example.www.DeleteFoodResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from deleteFood operation
           */
            public void receiveErrordeleteFood(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for createFood method
            * override this method for handling normal response from createFood operation
            */
           public void receiveResultcreateFood(
                    org.example.www.CreateFoodResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from createFood operation
           */
            public void receiveErrorcreateFood(java.lang.Exception e) {
            }
                


    }
    