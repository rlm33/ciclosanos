/**
 * FoodSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */
package org.example.www.food;

import org.example.www.CreateFood;
import org.example.www.CreateFoodResponse;
import org.example.www.DeleteFood;
import org.example.www.DeleteFoodResponse;
import org.example.www.Food;
import org.example.www.GetFood;
import org.example.www.GetFoodResponse;
import org.example.www.UpdateFood;
import org.example.www.UpdateFoodResponse;
import org.example.www.repositories.IFoodRepository;
import org.example.www.repositories.sqlite.FoodSQLiteRepository;

/**
 * FoodSkeleton java skeleton for the axisService
 */
public class FoodSkeleton implements FoodSkeletonInterface {
	
	private final IFoodRepository foodRepository = new FoodSQLiteRepository();

	/**
	 * Auto generated method signature
	 * 
	 * @param getFood0
	 * @return getFoodResponse1
	 */

	public GetFoodResponse getFood(GetFood req) {
		GetFoodResponse ret = new GetFoodResponse();
		String foodName = req.getName();
		ret.setExists(false);
		Food target = this.foodRepository.GetByName(foodName);
		if (target != null) {
			ret.setFood(target);
			ret.setExists(true);
		}
		
		this.foodRepository.Close();
		
		return ret;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param updateFood2
	 * @return updateFoodResponse3
	 */

	public UpdateFoodResponse updateFood(UpdateFood req) {
		UpdateFoodResponse ret = new UpdateFoodResponse();
		
		boolean done = this.foodRepository.Update(req.getFood());
		ret.setDone(done);
		// TODO: Manage string error
		ret.setError("");
		
		return ret;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param deleteFood4
	 * @return deleteFoodResponse5
	 */

	public DeleteFoodResponse deleteFood(DeleteFood req) {
		DeleteFoodResponse ret = new DeleteFoodResponse();
		
		boolean done = this.foodRepository.DeleteByName(req.getName());
		ret.setDone(done);
		// TODO: Manage string error
		ret.setError("");
		
		return ret;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param createFood6
	 * @return createFoodResponse7
	 */

	public CreateFoodResponse createFood(CreateFood req) {
		CreateFoodResponse ret = new CreateFoodResponse();
		Food target = req.getFood();
		boolean done = this.foodRepository.Create(target);
		ret.setDone(done);
		// TODO: Manage string error
		ret.setError("");
		
		this.foodRepository.Close();
		
		return ret;
	}

}
