
/**
 * FoodSkeletonInterface.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */
    package org.example.www.food;
    /**
     *  FoodSkeletonInterface java skeleton interface for the axisService
     */
    public interface FoodSkeletonInterface {
     
         
        /**
         * Auto generated method signature
         * 
                                    * @param getFood
         */

        
                public org.example.www.GetFoodResponse getFood
                (
                  org.example.www.GetFood getFood
                 )
            ;
        
         
        /**
         * Auto generated method signature
         * 
                                    * @param updateFood
         */

        
                public org.example.www.UpdateFoodResponse updateFood
                (
                  org.example.www.UpdateFood updateFood
                 )
            ;
        
         
        /**
         * Auto generated method signature
         * 
                                    * @param deleteFood
         */

        
                public org.example.www.DeleteFoodResponse deleteFood
                (
                  org.example.www.DeleteFood deleteFood
                 )
            ;
        
         
        /**
         * Auto generated method signature
         * 
                                    * @param createFood
         */

        
                public org.example.www.CreateFoodResponse createFood
                (
                  org.example.www.CreateFood createFood
                 )
            ;
        
         }
    