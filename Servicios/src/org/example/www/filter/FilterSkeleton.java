package org.example.www.filter;

import java.util.ArrayList;

import org.example.www.FilterLists;
import org.example.www.FilterListsResponse;

/**
 * FilterSkeleton java skeleton for the axisService
 */
public class FilterSkeleton implements FilterSkeletonInterface {

	/**
	 * Auto generated method signature
	 * 
	 * @param filterLists0
	 * @return filterListsResponse1
	 */

	public FilterListsResponse filterLists(FilterLists req) {
		
		FilterListsResponse resp = new FilterListsResponse();
		
		String[] listComplete = req.getCompleteList();
		String[] listFilter = req.getFilterList();
		ArrayList<String> arrayResult = new ArrayList<String>();
		boolean find=false;
		
		// Detectamos los valores que se repiten
		for (String complete : listComplete) {
			find=false;
			for (String filter : listFilter) {
				if(filter.equals(complete)){
					find=true;
				}
			}
			if(!find){
				arrayResult.add(complete);
			}
		}
		
		//Parseamos el array a String[] y se lo pasamos a resp
		String[] listResult = new String[arrayResult.size()];
		for(int i=0;i<arrayResult.size();i++){
			listResult[i] = arrayResult.get(i);
		}
		
		resp.setFilteredList(listResult);
		
		return resp;
	}

}