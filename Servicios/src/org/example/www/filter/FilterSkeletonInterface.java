
/**
 * FilterSkeletonInterface.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */
    package org.example.www.filter;
    /**
     *  FilterSkeletonInterface java skeleton interface for the axisService
     */
    public interface FilterSkeletonInterface {
     
         
        /**
         * Auto generated method signature
         * 
                                    * @param filterLists
         */

        
                public org.example.www.FilterListsResponse filterLists
                (
                  org.example.www.FilterLists filterLists
                 )
            ;
        
         }
    