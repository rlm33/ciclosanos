
/**
 * NutritionistCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */

    package org.example.www.nutritionist;

    /**
     *  NutritionistCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class NutritionistCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public NutritionistCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public NutritionistCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for deleteNutritionist method
            * override this method for handling normal response from deleteNutritionist operation
            */
           public void receiveResultdeleteNutritionist(
                    org.example.www.DeleteNutritionistResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from deleteNutritionist operation
           */
            public void receiveErrordeleteNutritionist(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for createNutritionist method
            * override this method for handling normal response from createNutritionist operation
            */
           public void receiveResultcreateNutritionist(
                    org.example.www.CreateNutritionistResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from createNutritionist operation
           */
            public void receiveErrorcreateNutritionist(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for updateNutritionist method
            * override this method for handling normal response from updateNutritionist operation
            */
           public void receiveResultupdateNutritionist(
                    org.example.www.UpdateNutritionistResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from updateNutritionist operation
           */
            public void receiveErrorupdateNutritionist(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getNutritionist method
            * override this method for handling normal response from getNutritionist operation
            */
           public void receiveResultgetNutritionist(
                    org.example.www.GetNutritionistResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getNutritionist operation
           */
            public void receiveErrorgetNutritionist(java.lang.Exception e) {
            }
                


    }
    