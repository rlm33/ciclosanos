
/**
 * NutritionistSkeletonInterface.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */
    package org.example.www.nutritionist;
    /**
     *  NutritionistSkeletonInterface java skeleton interface for the axisService
     */
    public interface NutritionistSkeletonInterface {
     
         
        /**
         * Auto generated method signature
         * 
                                    * @param deleteNutritionist
         */

        
                public org.example.www.DeleteNutritionistResponse deleteNutritionist
                (
                  org.example.www.DeleteNutritionist deleteNutritionist
                 )
            ;
        
         
        /**
         * Auto generated method signature
         * 
                                    * @param createNutritionist
         */

        
                public org.example.www.CreateNutritionistResponse createNutritionist
                (
                  org.example.www.CreateNutritionist createNutritionist
                 )
            ;
        
         
        /**
         * Auto generated method signature
         * 
                                    * @param updateNutritionist
         */

        
                public org.example.www.UpdateNutritionistResponse updateNutritionist
                (
                  org.example.www.UpdateNutritionist updateNutritionist
                 )
            ;
        
         
        /**
         * Auto generated method signature
         * 
                                    * @param getNutritionist
         */

        
                public org.example.www.GetNutritionistResponse getNutritionist
                (
                  org.example.www.GetNutritionist getNutritionist
                 )
            ;
        
         }
    