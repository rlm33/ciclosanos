/**
 * NutritionistSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */
package org.example.www.nutritionist;

import org.example.www.CreateNutritionist;
import org.example.www.CreateNutritionistResponse;
import org.example.www.DeleteNutritionist;
import org.example.www.DeleteNutritionistResponse;
import org.example.www.Nutritionist;
import org.example.www.GetNutritionistResponse;
import org.example.www.GetNutritionist;
import org.example.www.UpdateNutritionist;
import org.example.www.UpdateNutritionistResponse;
import org.example.www.repositories.INutritionistRepository;
import org.example.www.repositories.sqlite.NutritionistSQLiteRepository;

/**
 * NutritionistSkeleton java skeleton for the axisService
 */
public class NutritionistSkeleton implements NutritionistSkeletonInterface {

	private final INutritionistRepository nutritionistRepository = new NutritionistSQLiteRepository();
	
	/**
	 * Auto generated method signature
	 * 
	 * @param deleteNutritionist0
	 * @return deleteNutritionistResponse1
	 */

	public DeleteNutritionistResponse deleteNutritionist(DeleteNutritionist req) {
		DeleteNutritionistResponse ret = new DeleteNutritionistResponse();
		
		boolean done = this.nutritionistRepository.DeleteByEmail(req.getEmail());
		ret.setDone(done);
		// TODO: Manage string error
		ret.setError("");
		
		return ret;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param createNutritionist2
	 * @return createNutritionistResponse3
	 */

	public CreateNutritionistResponse createNutritionist(CreateNutritionist req) {
		CreateNutritionistResponse ret = new CreateNutritionistResponse();
		Nutritionist target = req.getNutritionist();
		boolean done = this.nutritionistRepository.Create(target);
		ret.setDone(done);
		// TODO: Manage string error
		ret.setError("");
		
		this.nutritionistRepository.Close();
		
		return ret;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param updateNutritionist4
	 * @return updateNutritionistResponse5
	 */

	public UpdateNutritionistResponse updateNutritionist(UpdateNutritionist req) {
		UpdateNutritionistResponse ret = new UpdateNutritionistResponse();
		
		boolean done = this.nutritionistRepository.Update(req.getNutritionist());
		ret.setDone(done);
		// TODO: Manage string error
		ret.setError("");
		
		return ret;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param getNutritionist6
	 * @return getNutritionistResponse7
	 */

	public GetNutritionistResponse getNutritionist(GetNutritionist req) {
		GetNutritionistResponse ret = new GetNutritionistResponse();
		String nutritionistName = req.getEmail();
		ret.setDone(false);
		Nutritionist target = this.nutritionistRepository.GetByEmail(nutritionistName);
		if (target != null) {
			ret.setNutritionist(target);
			ret.setDone(true);
		}
		
		this.nutritionistRepository.Close();
		
		return ret;
	}

}
